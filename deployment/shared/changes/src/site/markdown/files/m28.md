# Changes from M27 to M28

<!-- MACRO{toc} -->

## Applications

* Tikal

    * Fixed [issue #444](https://bitbucket.org/okapiframework/okapi/issues/444): Now the `-imp` command can use the `-approved`
      option to import only approved entries.

* Rainbow

    * Fixed [issue #464](https://bitbucket.org/okapiframework/okapi/issues/464): Exported batch configurations should now
      include default filter mappings for all known extensions.
    * Files with the `.sdlxliff` extension will now use `okf_xliff-sdl`
      filter configuration by default.
    * Fixed help page for command-line `-?`

## Libraries

    * Fixed [issue #442](https://bitbucket.org/okapiframework/okapi/issues/442) where the blacklist for the term checker was
      not working for Japanese.
    * Fixed [issue #439](https://bitbucket.org/okapiframework/okapi/issues/439): Quality issues based on patterns containing
      newline characters did not display correctly in CheckMate.
    * Fixed issue in the verification library where the check for
      leading and trailing whitespace did not take into account empty
      string, causing an index out of range error.
    * Fixed [issue #456](https://bitbucket.org/okapiframework/okapi/issues/456): SRX issue with empty "beforebreak" in rule.
    * Changed SWT libraries to version 4.4

## Filters

* OpenXML Filter

    * Fixed [issue #165](https://bitbucket.org/okapiframework/okapi/issues/165): Strings in Excel files are now extracted in
      a more logical order (sheet-by-sheet, one row at a time, ordered
      by columns). Additionally, strings that appear multiple times in
      are now exposed for translation once for each occurrence, and
      may be translated independently. **Warning**: these changes
      may create problems merging translated content from Excel files
      that were processed with previous versions of Okapi.
    * Fixed [issue #338](https://bitbucket.org/okapiframework/okapi/issues/338): the "Exclude color" configuration option is
      now correctly applied to Excel files. This option works for cell
      background colors only. The colors available in the Rainbow
      filter configuration UI are aligned against the Excel 2011
      "standard colors"; additional RGB values may be excluded by
      editing your filter configuration file directly.
    * Fixed [issue #390](https://bitbucket.org/okapiframework/okapi/issues/390): the "Exclude column" configuration option is
      now correctly applied to Excel files.
    * Fixed [issue #440](https://bitbucket.org/okapiframework/okapi/issues/440): markers for spelling and grammar errors are
      now stripped when exposing text for translation.
    * Fixed [issue #441](https://bitbucket.org/okapiframework/okapi/issues/441): added new options to expose line breaks and
      tabs for translation as literal characters.
    * Fixed [issue #443](https://bitbucket.org/okapiframework/okapi/issues/443): added the "Exclude Graphical Metadata"
      option to prevent metadata associated from graphics and
      textboxes from being exposed for translation.
    * Fixed [issue #447](https://bitbucket.org/okapiframework/okapi/issues/447): the "Translate Document Properties" option
      will now work for PowerPoint files.
    * Fixed [issue #448](https://bitbucket.org/okapiframework/okapi/issues/448): the "Translate Comments" option will now
      work for PowerPoint files.
    * Fixed [issue #449](https://bitbucket.org/okapiframework/okapi/issues/449): the "Translate Slide Masters" option will
      now also expose master layout content that is used by slides in
      the document.
    * Fixed [issue #451](https://bitbucket.org/okapiframework/okapi/issues/451): the "Translate Document Properties" option
      will now work for Excel files.
    * Fixed [issue #452](https://bitbucket.org/okapiframework/okapi/issues/452): Word files containing nested graphicData
      sections are no longer corrupted during processing.
    * Fixed [issue #453](https://bitbucket.org/okapiframework/okapi/issues/453): Word files that do not contain a `word/styles.xml`
      part can now be processed.
    * Fixed [issue #454](https://bitbucket.org/okapiframework/okapi/issues/454): entities occurring in alternateContent
      sections of Word documents are now handled correctly.
    * Fixed [issue #457](https://bitbucket.org/okapiframework/okapi/issues/457): empty lines in Word files are sometimes
      stripped.
    * Fixed [issue #458](https://bitbucket.org/okapiframework/okapi/issues/458): target text is lost in complicated run
      structures
    * Fixed [issue #467](https://bitbucket.org/okapiframework/okapi/issues/467): tabs in Word files are sometimes stripped.
    * Fixed [issue #473](https://bitbucket.org/okapiframework/okapi/issues/473): deletion change tracking can cause target
      corruption.
    * Fixed [issue #474](https://bitbucket.org/okapiframework/okapi/issues/474): files containing formulas could become
      corrupted.
    * Fixed [issue #476](https://bitbucket.org/okapiframework/okapi/issues/476): insertion change tracking can cause target
      corruption.
    * Fixed [issue #482](https://bitbucket.org/okapiframework/okapi/issues/482): `<w:lang>` tags are now
      stripped during extraction.
    * Fixed [issue #484](https://bitbucket.org/okapiframework/okapi/issues/484): Added a "Clean Tags Aggressively" option to
      the filter. When this option is enabled, the filter will strip
      certain types of formatting markup (whitespace and vertical
      alignment adjustment) that is spuriously inserted when
      converting other formats (such as PDF) to OpenXML. This produces
      smoother segmentation in some cases.
    * Fixed [issue #485](https://bitbucket.org/okapiframework/okapi/issues/485): Strip machine-generated "_GoBack" bookmarks
      from Word files.

* ICML Filter

    * Added the ICML Filter for WCML files.

* HTML Filter

    * Added support for ASPX comments and fixed tag-like attribute
      values extra output.

* PO Filter

    * A `Plural-Forms:` header that declares `nplurals=1`
      is now handled correclty.

* Table Filter

    * Blank lines inside qualified CSV cells are now preserved.
    * CSV text qualifiers can now be optionally added on output when
      required to maintain well-formedness.

## Steps

* Rainbow Translation Kit Creation

    * Improved support for XLIFF 2 packages

* Rainbow Translation Kit Merging

    * Improved support for XLIFF 2 packages

* Text Rewriting Step

    * Fixed the case where the target had only inline codes and the
      source text and inline codes. Now the base text is taken from
      the source.
    * Now expansion is done before the last inline code.
