# Changes from M33 to M34

<!-- MACRO{toc} -->

## Filters

* IDML Filter

    * Complete rewrite of the filter, fixing many problems and changing
      several behaviors. **Warning: files extracted with previous
      versions of Okapi will no longer merge successfully due to changes
      in text units and skeleton processing.** Resolves issues
      [#544](https://bitbucket.org/okapiframework/okapi/issues/544),
      [#497](https://bitbucket.org/okapiframework/okapi/issues/497),
      [#302](https://bitbucket.org/okapiframework/okapi/issues/302),
      [#378](https://bitbucket.org/okapiframework/okapi/issues/378),
      [#293](https://bitbucket.org/okapiframework/okapi/issues/293),
      [#259](https://bitbucket.org/okapiframework/okapi/issues/259),
      [#245](https://bitbucket.org/okapiframework/okapi/issues/245),
      [#471](https://bitbucket.org/okapiframework/okapi/issues/471),
      and [#317](https://bitbucket.org/okapiframework/okapi/issues/317). Additionally
      deprecates the filter configuration options "Maximum Spread Size",
      "Generate an error when a spread is larger than the specified
      value", and "Create new text units on hard returns".

* ITS Filter

    * The separator for the `annotatorsRef` values is now `'\s'`
      (white-space) rather than `' '` (ASCII space). Resolves
      [issue #612](https://bitbucket.org/okapiframework/okapi/issues/612).

* JSON Filter

    * Fixed [issue #410](https://bitbucket.org/okapiframework/okapi/issues/410): add an option to toggle whether or not `'/'` is
      escaped in output.

* OpenXML Filter

    * Fixed [issue #503](https://bitbucket.org/okapiframework/okapi/issues/503): chart content will now be exposed for
      translation in Excel and Powerpoint files.
    * Fixed [issue #614](https://bitbucket.org/okapiframework/okapi/issues/614): fixed a case where text with different styles
      could be incorrectly merged into a single run.

* XLIFF Filter

    * Fixed [issue #110](https://bitbucket.org/okapiframework/okapi/issues/110): `CDATA` sections that appear in markup will be
      preserved.

## Steps

* Terminology Leveraging

    * Added a step to annotate the source and target content with
      terminology information leveraged from a term base.

* XML Validation

    * Made using the DTDs found in the input documents an option (set by default).

## Connectors

* ModernMT API Connector

    * Added a connector for the [ModernMT Project](http://www.modernmt.eu/).

* Google MT

    * Improve retry behavior, particularly when exceeding the API rate limits.

## Applications

* Tikal

    * You can now use the `-mmt` option to use the
      [ModernMT API Connector](http://okapiframework.org/wiki/index.php?title=ModernMT_API_Connector).
    * Bugs in the usage of the `-lingo24` options have been fixed.

## Core

    * Fixed how `CDATA` is preserved when in the skeleton. Resolves [issue #624](https://bitbucket.org/okapiframework/okapi/issues/624).

## Libraries

* Quality Check

    * Added an option to show only relative paths in Quality Check
      report. Resolves [issue #616](https://bitbucket.org/okapiframework/okapi/issues/616).
    * Improved how some issues with localizable items were reported.

* General

    * The XLIFF 2 library has been upgraded to the version 1.1.6.
