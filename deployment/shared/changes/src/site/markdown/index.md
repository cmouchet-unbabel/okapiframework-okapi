# Okapi Framework Changes Log

Note that this document is common to both the okapi-lib distribution and the okapi-apps distribution. \
The information pertaining to applications other than Tikal are relevant only for the okapi-apps distribution.

Download page for latest stable release: https://okapiframework.org/binaries/main/1.43.0 \
Main Web site: https://okapiframework.org/wiki \
Project site (code): https://bitbucket.org/okapiframework/okapi
