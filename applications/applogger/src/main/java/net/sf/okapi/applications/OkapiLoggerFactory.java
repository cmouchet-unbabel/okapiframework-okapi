/*===========================================================================
  Copyright (C) 2021 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
===========================================================================*/

package net.sf.okapi.applications;

import org.slf4j.ILoggerFactory;
import org.slf4j.Logger;

public class OkapiLoggerFactory implements ILoggerFactory {
	/*
	 * For a proper implementation we would need a map from the logger name to the
	 * logger itself (cache-like).
	 *
	 * But we are not going to complicate our life here. We don't want (nor need) to
	 * re-implement all the fancy functionality of modern logging frameworks
	 * (appenders, encoder, layouts, filters, etc.)
	 *
	 * There are a couple of reasons we would want to change this to use a map: 1.
	 * To show the name of the logger in the output. The logger name is usually the
	 * name of the class doing the logging. 2. To filter logging from certain
	 * classes based on different levels.
	 *
	 * But neither Tikal nor Rainbow show the logger name, and don't filter.
	 *
	 * If we decide to do any of that, change this to a cache-style implementation.
	 */
	private static final OkapiLogger SINGLETON = new OkapiLogger("okapi_logger");

	@Override
	public Logger getLogger(String name) {
		return SINGLETON;
	}
}
