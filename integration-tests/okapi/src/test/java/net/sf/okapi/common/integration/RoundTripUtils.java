package net.sf.okapi.common.integration;

import net.sf.okapi.common.Event;
import net.sf.okapi.common.ISimplifierRulesParameters;
import net.sf.okapi.common.LocaleId;
import net.sf.okapi.common.Util;
import net.sf.okapi.common.filters.DefaultFilters;
import net.sf.okapi.common.filters.FilterConfigurationMapper;
import net.sf.okapi.common.filters.FilterTestDriver;
import net.sf.okapi.common.filterwriter.XLIFFWriter;
import net.sf.okapi.common.filterwriter.XLIFFWriterParameters;
import net.sf.okapi.common.pipelinedriver.BatchItemContext;
import net.sf.okapi.common.pipelinedriver.IPipelineDriver;
import net.sf.okapi.common.pipelinedriver.PipelineDriver;
import net.sf.okapi.common.resource.ITextUnit;
import net.sf.okapi.common.resource.RawDocument;
import net.sf.okapi.lib.merge.step.OriginalDocumentXliffMergerStep;
import net.sf.okapi.lib.serialization.step.OriginalDocumentTextUnitFlatMergerStep;
import net.sf.okapi.lib.serialization.writer.ProtoBufferTextUnitFlatWriter;
import net.sf.okapi.steps.common.FilterEventsWriterStep;
import net.sf.okapi.steps.common.RawDocumentToFilterEventsStep;
import net.sf.okapi.steps.common.RawDocumentWriterStep;
import net.sf.okapi.steps.common.codesimplifier.PostSegmentationCodeSimplifierStep;
import net.sf.okapi.steps.segmentation.Parameters;
import net.sf.okapi.steps.segmentation.SegmentationStep;
import net.sf.okapi.steps.whitespacecorrection.WhitespaceCorrectionStep;

import java.io.File;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.util.LinkedList;
import java.util.List;

public final class RoundTripUtils {
	private static String sourceSrx = RoundTripUtils.class.getClassLoader().getResource("okapi_default_icu4j.srx").getPath();
	private static String targetSrx = RoundTripUtils.class.getClassLoader().getResource("okapi_default_icu4j.srx").getPath();

	public static FilterConfigurationMapper extract(final LocaleId source, final LocaleId target, final String original, final String outputPath,
													final String configId, final String customConfigPath, final boolean serializedOutput) throws URISyntaxException {
		return extract(source, target, original, outputPath, configId, customConfigPath, serializedOutput, false);
	}

	public static FilterConfigurationMapper extract(final LocaleId source, final LocaleId target,
													final String originalPath, final String outputPath,
													final String configId, final String customConfigPath,
													final boolean serializedOutput, final boolean simplify) throws URISyntaxException {
		FilterConfigurationMapper mapper = createMapper(customConfigPath, configId);
		PipelineDriver driver = createInitialPipeline(source, target, configId, simplify, mapper);
		driver.setFilterConfigurationMapper(mapper);
		if (serializedOutput) {
			try (ProtoBufferTextUnitFlatWriter writer = new ProtoBufferTextUnitFlatWriter();
				 RawDocument originalDoc = new RawDocument(Util.toURI(originalPath), StandardCharsets.UTF_8.name(), source, target)) {

				// Filter events to raw document final step (using the defined writer)
				final FilterEventsWriterStep fewStep = new FilterEventsWriterStep();
				fewStep.setFilterWriter(writer);
				final net.sf.okapi.lib.serialization.writer.Parameters params = writer.getParameters();
				params.setCopySource(true);
				fewStep.setDocumentRoots(Util.getDirectoryName(originalPath));
				driver.addStep(fewStep);

				originalDoc.setFilterConfigId(configId);

				driver.addBatchItem(originalDoc, new File(outputPath).toURI(), StandardCharsets.UTF_8.name());

				// Process
				driver.processBatch();
			}
		} else {
			try (XLIFFWriter writer = new XLIFFWriter();
				 RawDocument originalDoc = new RawDocument(Util.toURI(originalPath), StandardCharsets.UTF_8.name(), source, target)) {

				// Filter events to raw document final step (using the defined writer)
				final FilterEventsWriterStep fewStep = new FilterEventsWriterStep();
				fewStep.setFilterWriter(writer);
				final XLIFFWriterParameters xliffParams = writer.getParameters();
				xliffParams.setPlaceholderMode(false);
				xliffParams.setIncludeAltTrans(true);
				xliffParams.setEscapeGt(true);
				xliffParams.setIncludeCodeAttrs(true);
				xliffParams.setCopySource(true);
				xliffParams.setIncludeIts(true);
				xliffParams.setIncludeNoTranslate(true);
				xliffParams.setToolId("okapi");
				xliffParams.setToolName("okapi-tests");
				xliffParams.setToolCompany("okapi");
				xliffParams.setToolVersion("M29");

				fewStep.setDocumentRoots(Util.getDirectoryName(originalPath));
				driver.addStep(fewStep);

				originalDoc.setFilterConfigId(configId);

				driver.addBatchItem(originalDoc, new File(outputPath).toURI(), StandardCharsets.UTF_8.name());

				// Process
				driver.processBatch();
			}
		}

		return mapper;
	}

	private static FilterConfigurationMapper createMapper(final String customConfigPath, final String configId) throws URISyntaxException {
		final FilterConfigurationMapper mapper = new FilterConfigurationMapper();
		DefaultFilters.setMappings(mapper, false, true);
		if (customConfigPath != null) {
			mapper.setCustomConfigurationsDirectory(customConfigPath);
			mapper.addCustomConfiguration(configId);
			// look for secondary filter config - only one per subdir
			final File secondary = IntegrationtestUtils.getSecondaryConfigFile(new File(customConfigPath), configId);
			if (secondary != null) {
				mapper.addCustomConfiguration(Util.getFilename(secondary.getName(), false));
			}
			mapper.updateCustomConfigurations();
		}
		return mapper;
	}

	private static PipelineDriver createInitialPipeline(final LocaleId source, final LocaleId target,
														final String configId, final boolean simplify,
														FilterConfigurationMapper mapper) {

		// Create the driver
		final PipelineDriver driver = new PipelineDriver();

		// Raw document to filter events step
		final RawDocumentToFilterEventsStep rd2feStep = new RawDocumentToFilterEventsStep();
		driver.addStep(rd2feStep);

		final SegmentationStep ss = new SegmentationStep();
		ss.setSourceLocale(source);
		final List<LocaleId> tl = new LinkedList<>();
		tl.add(target);
		ss.setTargetLocales(tl);
		final Parameters params = ss.getParameters();
		params.setSegmentSource(true);
		params.setSegmentTarget(true);
		params.setSourceSrxPath(sourceSrx);
		params.setTargetSrxPath(targetSrx);
		driver.addStep(ss);

		if (simplify) {
			net.sf.okapi.steps.common.codesimplifier.Parameters p = new net.sf.okapi.steps.common.codesimplifier.Parameters();
			ISimplifierRulesParameters fp = (ISimplifierRulesParameters) mapper.getConfiguration(configId).parameters;
			if (fp != null && null != fp.getSimplifierRules()) {
				p.setRules(fp.getSimplifierRules());
			}

			PostSegmentationCodeSimplifierStep simplifier = new PostSegmentationCodeSimplifierStep();
			simplifier.setParameters(p);
			driver.addStep(simplifier);
		}

		return driver;
	}

	public static void merge(final LocaleId source, final LocaleId target, final String originalPath,
							 final String extractedPath, final String outputPath, final String configId,
							 FilterConfigurationMapper mapper, final boolean serializedOutput) throws URISyntaxException {
		try (RawDocument originalDoc = new RawDocument(Util.toURI(originalPath), StandardCharsets.UTF_8.name(), source, target);
				RawDocument xlfDoc = new RawDocument(Util.toURI(extractedPath), StandardCharsets.UTF_8.name(), source, target)) {
			originalDoc.setFilterConfigId(configId);

			final IPipelineDriver driver = new PipelineDriver();  
			driver.setFilterConfigurationMapper(mapper);
			final BatchItemContext bic = new BatchItemContext(
					xlfDoc,
					Util.toURI(outputPath), 
					StandardCharsets.UTF_8.name(), 
					originalDoc);
			driver.addBatchItem(bic);
			driver.addStep(new WhitespaceCorrectionStep());
			if (serializedOutput) {
				driver.addStep(new OriginalDocumentTextUnitFlatMergerStep());
			} else {
				// Make sure that whitespace is preserved
				OriginalDocumentXliffMergerStep originalDocumentXliffMergerStep = new OriginalDocumentXliffMergerStep();
				net.sf.okapi.lib.merge.step.Parameters parameters = new net.sf.okapi.lib.merge.step.Parameters();
				parameters.setPreserveWhiteSpaceByDefault(true);
				originalDocumentXliffMergerStep.setParameters(parameters);
				driver.addStep(originalDocumentXliffMergerStep);
			}
			driver.addStep(new RawDocumentWriterStep());
			driver.processBatch();
			driver.destroy();
		}
	}

	/**
	 * Assumes merge has been called to set source and target locales
	 */
	public static boolean compareEvents(final List<Event> actual, final List<Event> expected, 
			final boolean includeSkeleton, final boolean ignoreSkelWhitespace,
										final boolean ignoreFragmentWhitespace, boolean ignoreSegmentation) {
		final boolean r =  FilterTestDriver.compareEvents(actual, expected, includeSkeleton, ignoreSkelWhitespace,
				ignoreFragmentWhitespace, ignoreSegmentation);
		return r;
	}

	/**
	 * Assumes merge has been called to set source and target locales
	 */
	public static boolean compareTextUnits(final List<ITextUnit> actual, final List<ITextUnit> expected,
										   final boolean ignoreFragmentWhitespace) {
		final boolean r =  FilterTestDriver.compareTextUnits(actual, expected, ignoreFragmentWhitespace);
		return r;
	}
}
