package net.sf.okapi.simplifier.integration;

import net.sf.okapi.common.integration.FileComparator;
import net.sf.okapi.common.integration.SimplifyRoundTripIT;
import net.sf.okapi.filters.openoffice.OpenOfficeFilter;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.io.FileNotFoundException;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.List;

@RunWith(JUnit4.class)
public class RoundTripSimplifyOpenOfficeIT extends SimplifyRoundTripIT {
	private static final String CONFIG_ID = "okf_openoffice";
	private static final String DIR_NAME = "/openoffice/";
	private static final List<String> EXTENSIONS = Arrays.asList(".odt", ".ods", ".odg", ".odp", ".ott", ".ots", ".otp", ".otg");
	private static final String XLIFF_EXTRACTED_EXTENSION = ".simplify_xliff";

	public RoundTripSimplifyOpenOfficeIT() {
		super(CONFIG_ID, DIR_NAME, EXTENSIONS, XLIFF_EXTRACTED_EXTENSION, OpenOfficeFilter::new);
	}

	@Test
	public void openOfficeFiles() throws FileNotFoundException, URISyntaxException {
		setSerializedOutput(false);
		realTestFiles(false, new FileComparator.ArchiveComparator());
	}

	@Test
	public void openOfficeFilesSerialized() throws FileNotFoundException, URISyntaxException {
		setSerializedOutput(true);
		realTestFiles(false, new FileComparator.ArchiveComparator());
	}
}
