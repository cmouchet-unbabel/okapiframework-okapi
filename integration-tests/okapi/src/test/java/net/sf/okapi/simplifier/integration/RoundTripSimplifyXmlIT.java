package net.sf.okapi.simplifier.integration;

import net.sf.okapi.common.integration.SimplifyRoundTripIT;
import net.sf.okapi.common.integration.FileComparator;
import net.sf.okapi.filters.xml.XMLFilter;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.io.FileNotFoundException;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.List;

@RunWith(JUnit4.class)
public class RoundTripSimplifyXmlIT extends SimplifyRoundTripIT {
	private static final String CONFIG_ID = "okf_xml";
	private static final String DIR_NAME = "/xml/";
	private static final List<String> EXTENSIONS = Arrays.asList(".xml");
	private static final String XLIFF_EXTRACTED_EXTENSION = ".xliff";

	public RoundTripSimplifyXmlIT() {
		super(CONFIG_ID, DIR_NAME, EXTENSIONS, XLIFF_EXTRACTED_EXTENSION, XMLFilter::new);
	}

	@Test
	public void xmlFiles() throws FileNotFoundException, URISyntaxException {
		setSerializedOutput(false);
		realTestFiles(false, new FileComparator.XmlComparator());
	}

	@Test
	public void xmlFilesSerialized() throws FileNotFoundException, URISyntaxException {
		setSerializedOutput(true);
		realTestFiles(false, new FileComparator.XmlComparator());
	}
}
