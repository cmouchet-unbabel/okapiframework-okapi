package net.sf.okapi.xliffcompare.integration;

import net.sf.okapi.common.LocaleId;
import net.sf.okapi.common.integration.XliffCompareIT;
import net.sf.okapi.common.integration.FileComparator;
import net.sf.okapi.filters.xliff2.XLIFF2Filter;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.io.FileNotFoundException;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.List;

@RunWith(JUnit4.class)
public class Xliff2XliffCompareIT extends XliffCompareIT {
	private static final String CONFIG_ID = "okf_xliff2";
	private static final String DIR_NAME = "/xliff2/";
	private static final List<String> EXTENSIONS = Arrays.asList(".xliff", ".xlf", ".xlf2");

	public Xliff2XliffCompareIT() {
		super(CONFIG_ID, DIR_NAME, EXTENSIONS, LocaleId.EMPTY, XLIFF2Filter::new);
	}

	@Test
	public void xliff2XliffCompareFiles() throws FileNotFoundException, URISyntaxException {
		realTestFiles(true, new FileComparator.XmlComparator());
	}
}
