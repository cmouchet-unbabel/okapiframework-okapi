package net.sf.okapi.roundtrip.integration;

import net.sf.okapi.common.FileLocation;
import net.sf.okapi.common.filters.IFilter;
import net.sf.okapi.common.integration.EventRoundTripIT;
import net.sf.okapi.common.integration.FileComparator;
import net.sf.okapi.filters.idml.IDMLFilter;
import net.sf.okapi.filters.openxml.OpenXMLFilter;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.io.File;
import java.io.FileNotFoundException;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.List;
import java.util.function.Supplier;

@RunWith(JUnit4.class)
public class RoundTripOpenXmlIT extends EventRoundTripIT {
	private static final String CONFIG_ID = "okf_openxml";
	private static final String DIR_NAME = "/openxml/";
	private static final List<String> EXTENSIONS = Arrays.asList(".docx", ".pptx", ".xlsx");
	private static final Supplier<IFilter> FILTER_CONSTRUCTOR = OpenXMLFilter::new;
	final static FileLocation root = FileLocation.fromClass(RoundTripOpenXmlIT.class);
	public RoundTripOpenXmlIT() {
		super(CONFIG_ID, DIR_NAME, EXTENSIONS, OpenXMLFilter::new);
		addKnownFailingFile("Conference_Talk.pptx");
		addKnownFailingFile("big.docx");
		addKnownFailingFile("TestDako2.docx");
		addKnownFailingFile("offce_2013_big.docx");
		addKnownFailingFile("DiamondClarity_4Cs Education Source.docx");
	}

	@Ignore
	public void debug() throws FileNotFoundException, URISyntaxException {
		setSerializedOutput(true);
		setExtensions(EXTENSIONS);
		final File file =
				root.in("/openxml/docx/OpenXML_text_reference_v1_2.docx").asFile();
		runTest(new TestJob(CONFIG_ID, false, file, null, null,
				new FileComparator.EventComparator(), FILTER_CONSTRUCTOR));
	}

	@Test
	public void openXmlFiles() throws FileNotFoundException, URISyntaxException {
		setSerializedOutput(false);
		realTestFiles(false, new FileComparator.EventComparator());
	}

	@Test
	public void openXmlSerializedFiles() throws FileNotFoundException, URISyntaxException {
		setSerializedOutput(true);
		realTestFiles(false, new FileComparator.EventComparator());
	}
}
