/*
 * =============================================================================
 * Copyright (C) 2010-2021 by the Okapi Framework contributors
 * -----------------------------------------------------------------------------
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =============================================================================
 */
package net.sf.okapi.filters.openxml;

import net.sf.okapi.common.ParametersString;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import javax.xml.stream.XMLInputFactory;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(JUnit4.class)
public class WorksheetConfigurationsTest {
    @Test
    public void constructedFromParametersString() {
        final WorksheetConfigurations wcs = new WorksheetConfigurations.Default(new LinkedList<>());
        wcs.addFrom(
            new ParametersStringWorksheetConfigurationsInput(
                new ParametersString(
                "worksheetConfigurations.0.namePattern=\n" +
                    "worksheetConfigurations.0.excludedRows=\n" +
                    "worksheetConfigurations.0.excludedColumns=\n" +
                    "worksheetConfigurations.0.metadataRows=\n" +
                    "worksheetConfigurations.0.metadataColumns=\n" +
                    "worksheetConfigurations.1.namePattern=Sheet1\n" +
                    "worksheetConfigurations.1.excludedRows=1,2\n" +
                    "worksheetConfigurations.1.excludedColumns=A,B\n" +
                    "worksheetConfigurations.1.metadataRows=3,4\n" +
                    "worksheetConfigurations.1.metadataColumns=C,D\n" +
                    "worksheetConfigurations.2.namePattern=Sheet2\n" +
                    "worksheetConfigurations.2.excludedRows=1,2\n" +
                    "worksheetConfigurations.2.excludedColumns=A,B\n" +
                    "worksheetConfigurations.2.metadataRows=3,4\n" +
                    "worksheetConfigurations.2.metadataColumns=C,D\n" +
                    "worksheetConfigurations.3.namePattern=Sheet3\n" +
                    "worksheetConfigurations.3.excludedRows=1,2\n" +
                    "worksheetConfigurations.3.metadataRows=3,4\n" +
                    "worksheetConfigurations.4.namePattern=Sheet4\n" +
                    "worksheetConfigurations.4.excludedColumns=A,B\n" +
                    "worksheetConfigurations.4.metadataColumns=C,D\n" +
                    "worksheetConfigurations.5.namePattern=Sheet5\n" +
                    "worksheetConfigurations.number.i=6"
                )
            )
        );
        assertThat(wcs.excludedRowsFor("").size()).isEqualTo(0);
        assertThat(wcs.excludedColumnsFor("").size()).isEqualTo(0);
        assertThat(wcs.metadataRowsFor("").size()).isEqualTo(0);
        assertThat(wcs.metadataColumnsFor("").size()).isEqualTo(0);

        assertThat(wcs.excludedRowsFor("Sheet1").size()).isEqualTo(2);
        assertThat(wcs.excludedColumnsFor("Sheet1").size()).isEqualTo(2);
        assertThat(wcs.metadataRowsFor("Sheet1").size()).isEqualTo(2);
        assertThat(wcs.metadataColumnsFor("Sheet1").size()).isEqualTo(2);

        assertThat(wcs.excludedRowsFor("Sheet2").size()).isEqualTo(2);
        assertThat(wcs.excludedColumnsFor("Sheet2").size()).isEqualTo(2);
        assertThat(wcs.metadataRowsFor("Sheet2").size()).isEqualTo(2);
        assertThat(wcs.metadataColumnsFor("Sheet2").size()).isEqualTo(2);

        assertThat(wcs.excludedRowsFor("Sheet3").size()).isEqualTo(2);
        assertThat(wcs.excludedColumnsFor("Sheet3").size()).isEqualTo(0);
        assertThat(wcs.metadataRowsFor("Sheet3").size()).isEqualTo(2);
        assertThat(wcs.metadataColumnsFor("Sheet3").size()).isEqualTo(0);

        assertThat(wcs.excludedRowsFor("Sheet4").size()).isEqualTo(0);
        assertThat(wcs.excludedColumnsFor("Sheet4").size()).isEqualTo(2);
        assertThat(wcs.metadataRowsFor("Sheet4").size()).isEqualTo(0);
        assertThat(wcs.metadataColumnsFor("Sheet4").size()).isEqualTo(2);

        assertThat(wcs.excludedRowsFor("Sheet5").size()).isEqualTo(0);
        assertThat(wcs.excludedColumnsFor("Sheet5").size()).isEqualTo(0);
        assertThat(wcs.metadataRowsFor("Sheet5").size()).isEqualTo(0);
        assertThat(wcs.metadataColumnsFor("Sheet5").size()).isEqualTo(0);
    }

    @Test
    public void constructedFromExcelExcludedColumns() {
        final ConditionalParameters cp = new ConditionalParameters();
        cp.fromString(
            "tsExcelExcludedColumns.i=2\n" +
            "zzz0=1A\n" +
            "zzz1=B\n"
        );
        final WorksheetConfigurations wcs = cp.worksheetConfigurations();
        wcs.addFrom(
            new ExcelExcludedColumnsWorksheetConfigurationsInput(
                new WorkbookFragments.Default(
                    cp,
                    new Relationships(XMLInputFactory.newFactory())
                ),
                cp.tsExcelExcludedColumns
            )
        );
        assertThat(wcs.excludedRowsFor("").size()).isEqualTo(0);
        assertThat(wcs.excludedColumnsFor("").size()).isEqualTo(2);
        assertThat(wcs.metadataRowsFor("").size()).isEqualTo(0);
        assertThat(wcs.metadataColumnsFor("").size()).isEqualTo(0);

        assertThat(wcs.excludedColumnsFor("AnyName").size()).isEqualTo(1);
    }

    @Test
    public void exposedAsString() {
        final WorksheetConfigurations wcs = new WorksheetConfigurations.Default(
            new WorksheetConfiguration.Default("", Collections.emptyList(), Collections.emptyList(),Collections.emptyList(), Collections.emptyList()),
            new WorksheetConfiguration.Default("", Arrays.asList(1, 2), Arrays.asList("A", "B"), Arrays.asList(3, 4), Arrays.asList("C", "D")),
            new WorksheetConfiguration.Default("Sheet2", Arrays.asList(1, 2), Arrays.asList("A", "B"), Arrays.asList(3, 4), Arrays.asList("C", "D")),
            new WorksheetConfiguration.Default("Sheet3", Arrays.asList(1, 2), Collections.emptyList(), Arrays.asList(3, 4), Collections.emptyList()),
            new WorksheetConfiguration.Default("Sheet4", Collections.emptyList(), Arrays.asList("A", "B"), Collections.emptyList(), Arrays.asList("C", "D")),
            new WorksheetConfiguration.Default("Sheet5", Collections.emptyList(), Collections.emptyList(), Collections.emptyList(), Collections.emptyList())
        );
        assertThat(
            wcs.writtenTo(new ParametersStringWorksheetConfigurationsOutput()).toString())
            .isEqualTo(
                "#v1\n" +
                "worksheetConfigurations.0.namePattern=\n" +
                "worksheetConfigurations.1.namePattern=\n" +
                "worksheetConfigurations.1.excludedRows=1,2\n" +
                "worksheetConfigurations.1.excludedColumns=A,B\n" +
                "worksheetConfigurations.1.metadataRows=3,4\n" +
                "worksheetConfigurations.1.metadataColumns=C,D\n" +
                "worksheetConfigurations.2.namePattern=Sheet2\n" +
                "worksheetConfigurations.2.excludedRows=1,2\n" +
                "worksheetConfigurations.2.excludedColumns=A,B\n" +
                "worksheetConfigurations.2.metadataRows=3,4\n" +
                "worksheetConfigurations.2.metadataColumns=C,D\n" +
                "worksheetConfigurations.3.namePattern=Sheet3\n" +
                "worksheetConfigurations.3.excludedRows=1,2\n" +
                "worksheetConfigurations.3.metadataRows=3,4\n" +
                "worksheetConfigurations.4.namePattern=Sheet4\n" +
                "worksheetConfigurations.4.excludedColumns=A,B\n" +
                "worksheetConfigurations.4.metadataColumns=C,D\n" +
                "worksheetConfigurations.5.namePattern=Sheet5\n" +
                "worksheetConfigurations.number.i=6"
            );
    }
}
