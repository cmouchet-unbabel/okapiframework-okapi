/*===========================================================================
  Copyright (C) 2016-2017 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
===========================================================================*/

package net.sf.okapi.filters.openxml;

import static net.sf.okapi.filters.openxml.ContentTypes.Types.Common.CORE_PROPERTIES_TYPE;
import static net.sf.okapi.filters.openxml.ContentTypes.Types.Common.PACKAGE_RELATIONSHIPS;

import java.io.IOException;
import java.io.Reader;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.zip.ZipEntry;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.XMLEvent;

import net.sf.okapi.common.Event;
import net.sf.okapi.filters.openxml.ContentTypes.Types.Drawing;
import net.sf.okapi.filters.openxml.ContentTypes.Types.Powerpoint;
import net.sf.okapi.filters.openxml.Relationships.Rel;

class PowerpointDocument implements Document {
	private static final String SLIDE_MASTER = "/slideMaster";
	private static final String SLIDE_LAYOUT = "/slideLayout";
	private static final String COMMENTS = "/comments";
	private static final String NOTES_SLIDE = "/notesSlide";
	private static final String NOTES_MASTER = "/notesMaster";
	private static final String CHART = "/chart";
	private static final String DIAGRAM_DATA = "/diagramData";

	private static final Pattern RELS_NAME_PATTERN = Pattern.compile(".+slide\\d+\\.xml\\.rels");
	private Matcher relsNameMatcher = RELS_NAME_PATTERN.matcher("").reset();

	private static final String SLD = "sld";
	private static final QName SHOW = new QName("show");

	private final Document.General generalDocument;
	private PresentationFragments presentationFragments;
	private Enumeration<? extends ZipEntry> entries;
	private Set<String> hiddenSlides;
	private Set<String> slideLayoutNames;
	private Set<String> notesSlideNames;
	private Map<String, SlideFragments> slideMasterFragmentsByName;
	private Map<String, SlideFragments> slideLayoutFragmentsByName;
	private Map<String, SlideFragments> slideFragmentsByName;
	private Map<String, SlideFragments> notesMasterFragmentsByName;
	private Map<String, SlideFragments> notesSlideFragmentsByName;
	private Map<String, Set<String>> slideLayoutsBySlide;
	private Map<String, Set<String>> slideMastersBySlideLayout;
	private Map<String, String> slidesByComment;
	private Map<String, String> slidesByNotesSlide;
	private Map<String, Set<String>> notesMastersByNotesSlide;
	private Map<String, String> slidesByChart;
	private Map<String, String> slidesByDiagramData;

	PowerpointDocument(final Document.General generalDocument) {
		this.generalDocument = generalDocument;
	}

	@Override
	public Event open() throws IOException, XMLStreamException {
		this.presentationFragments = presentationFragments();
		this.entries = entries();
		this.hiddenSlides = hiddenSlides();
		this.slideLayoutNames = slideRelatedPartNamesOf(SLIDE_LAYOUT);
		this.notesSlideNames = slideRelatedPartNamesOf(NOTES_SLIDE);
		this.slideMasterFragmentsByName = slideMasterFragments();
		this.slideLayoutFragmentsByName = slideLayoutFragments();
		this.slideFragmentsByName = slideFragments();
		this.notesMasterFragmentsByName = notesMasterFragments();
		this.notesSlideFragmentsByName = notesSlideFragments();
		this.slideLayoutsBySlide = slideLayoutsBySlide();
		this.slideMastersBySlideLayout = slideMastersBySlideLayout();
		this.slidesByComment = slidesBy(COMMENTS);
		this.slidesByNotesSlide = slidesBy(NOTES_SLIDE);
		this.notesMastersByNotesSlide = notesMastersByNotesSlide();
		this.slidesByChart = slidesBy(CHART);
		this.slidesByDiagramData = slidesBy(DIAGRAM_DATA);

		return this.generalDocument.startDocumentEvent();
	}

	private PresentationFragments presentationFragments() throws IOException, XMLStreamException {
		final PresentationFragments pf = new PresentationFragments.Default(
			this.generalDocument.conditionalParameters(),
			this.generalDocument.eventFactory(),
			this.generalDocument.relationshipsFor(this.generalDocument.mainPartName())
		);
		try (final Reader reader = this.generalDocument.getPartReader(this.generalDocument.mainPartName())) {
			pf.readWith(this.generalDocument.inputFactory().createXMLEventReader(reader));
		}
		return pf;
	}

	/**
	 * Do additional reordering of the entries for PPTX files to make
	 * sure that slides are parsed in the correct order.  This is done
	 * by scraping information from one of the rels files and the
	 * presentation itself in order to determine the proper order, rather
	 * than relying on the order in which things appeared in the zip.
	 * @return the sorted enum of ZipEntry
	 * @throws IOException if any error is encountered while reading the stream
	 * @throws XMLStreamException if any error is encountered while parsing the XML
	 */
	private Enumeration<? extends ZipEntry> entries() throws IOException, XMLStreamException {
		Enumeration<? extends ZipEntry> entries = this.generalDocument.entries();
		List<? extends ZipEntry> entryList = Collections.list(entries);
		entryList.sort(new ZipEntryComparator(reorderedPartNames()));
		return Collections.enumeration(entryList);
	}

	private List<String> reorderedPartNames() throws IOException, XMLStreamException {
		final List<String> names = new LinkedList<>();
		for (final String slideName : this.presentationFragments.slideNames()) {
			names.add(slideName);
			if (this.generalDocument.conditionalParameters().getReorderPowerpointNotesAndComments()) {
				final String namespaceUri = this.generalDocument.documentRelationshipsNamespace().uri();
				names.addAll(slideRelationshipTargetsForType(slideName, namespaceUri.concat(NOTES_SLIDE)));
				names.addAll(slideRelationshipTargetsForType(slideName, namespaceUri.concat(COMMENTS)));
			}
		}
		return names;
	}

	private List<String> slideRelationshipTargetsForType(final String slideName, final String typeUri) throws IOException, XMLStreamException {
		List<Rel> rels = this.generalDocument.relationshipsFor(slideName).getRelByType(typeUri);
		return rels == null ? Collections.emptyList() :
				rels.stream()
						.map(r -> r.target)
						.collect(Collectors.toList());
	}

	private Set<String> hiddenSlides() throws IOException, XMLStreamException {
		final Set<String> hiddenSlides = new HashSet<>(this.presentationFragments.slideNames().size());
		for (final String name : this.presentationFragments.slideNames()) {
			try (final Reader reader = this.generalDocument.getPartReader(name)) {
				final XMLEventReader eventReader = this.generalDocument.inputFactory().createXMLEventReader(reader);
				final XMLEvent e = eventReader.nextTag();
				if (e.isStartElement()
					&& SLD.equals(e.asStartElement().getName().getLocalPart())
					&& !XMLEventHelpers.getBooleanAttributeValue(
					e.asStartElement(),
					SHOW,
					XMLEventHelpers.DEFAULT_BOOLEAN_ATTRIBUTE_TRUE_VALUE
				)
				) {
					hiddenSlides.add(name);
				}
			}
		}
		return hiddenSlides;
	}

	private Set<String> slideRelatedPartNamesOf(final String type) throws IOException, XMLStreamException {
		final Set<String> names = new HashSet<>();
		final String typeUri = this.generalDocument.documentRelationshipsNamespace().uri().concat(type);
		for (String slideName : this.presentationFragments.slideNames()) {
			final List<Relationships.Rel> rels =
				this.generalDocument.relationshipsFor(slideName).getRelByType(typeUri);
			if (null != rels && !rels.isEmpty()) {
				names.addAll(
					rels.stream()
						.map(rel -> rel.target)
						.collect(Collectors.toSet())
				);
			}
		}
		return names;
	}

	private Map<String, SlideFragments> slideMasterFragments() throws IOException, XMLStreamException {
		final Map<String, SlideFragments> slideMasterFragments =
			new HashMap<>(this.presentationFragments.slideMasterNames().size());
		for (final String name : this.presentationFragments.slideMasterNames()) {
			slideMasterFragments.put(name, slideMasterFragmentsOf(name));
		}
		return slideMasterFragments;
	}

	private SlideFragments slideMasterFragmentsOf(final String partName) throws IOException, XMLStreamException {
		try (final Reader reader = this.generalDocument.getPartReader(partName)) {
			final XMLEventReader eventReader =
				this.generalDocument.inputFactory().createXMLEventReader(reader);
			while (eventReader.hasNext()) {
				final XMLEvent e = eventReader.nextEvent();
				if (e.isEndElement() && SlideMasterFragments.SLD_MASTER.equals(e.asEndElement().getName().getLocalPart())) {
					break;
				}
				if (!e.isStartElement()) {
					continue;
				}
				if (SlideMasterFragments.SLD_MASTER.equals(e.asStartElement().getName().getLocalPart())) {
					final SlideTemplateFragments slideTemplateFragments = new SlideMasterFragments(
						e.asStartElement(),
						this.generalDocument.conditionalParameters(),
						this.generalDocument.eventFactory()
					);
					slideTemplateFragments.readWith(eventReader);
					return slideTemplateFragments;
				}
			}
		}
		return new SlideMasterFragments.Empty();
	}

	private Map<String, SlideFragments> slideLayoutFragments() throws IOException, XMLStreamException {
		final Map<String, SlideFragments> slideLayoutFragments = new HashMap<>(this.slideLayoutNames.size());
		for (final String name : this.slideLayoutNames) {
			slideLayoutFragments.put(name, slideLayoutFragmentsOf(name));
		}
		return slideLayoutFragments;
	}

	private SlideFragments slideLayoutFragmentsOf(final String partName) throws IOException, XMLStreamException {
		final List<Relationships.Rel> rels = this.generalDocument.relationshipsFor(partName)
			.getRelByType(
				this.generalDocument.documentRelationshipsNamespace().uri().concat(SLIDE_MASTER)
			);
		final SlideFragments slideMasterFragments =
			null != rels && !rels.isEmpty() && this.slideMasterFragmentsByName.containsKey(rels.get(0).target)
				? this.slideMasterFragmentsByName.get(rels.get(0).target)
				: new SlideTemplateFragments.Empty();
		try (final Reader reader = this.generalDocument.getPartReader(partName)) {
			final XMLEventReader eventReader = this.generalDocument.inputFactory().createXMLEventReader(reader);
			while (eventReader.hasNext()) {
				final XMLEvent e = eventReader.nextEvent();
				if (e.isEndElement() && SlideFragments.C_SLD.equals(e.asEndElement().getName().getLocalPart())) {
					break;
				}
				if (!e.isStartElement()) {
					continue;
				}
				if (SlideFragments.C_SLD.equals(e.asStartElement().getName().getLocalPart())) {
					final SlideFragments slideLayoutFragments = new SlideFragments.Default(
						e.asStartElement(),
						this.generalDocument.conditionalParameters(),
						this.generalDocument.eventFactory(),
						slideMasterFragments
					);
					slideLayoutFragments.readWith(eventReader);
					return slideLayoutFragments;
				}
			}
		}
		return new SlideFragments.Empty(slideMasterFragments);
	}

	private Map<String, SlideFragments> slideFragments() throws IOException, XMLStreamException {
		final Map<String, SlideFragments> slideFragments =
			new HashMap<>(this.presentationFragments.slideNames().size());
		for (final String name : this.presentationFragments.slideNames()) {
			slideFragments.put(name, slideFragmentsOf(name));
		}
		return slideFragments;
	}

	private SlideFragments slideFragmentsOf(final String partName) throws IOException, XMLStreamException {
		final List<Relationships.Rel> rels = this.generalDocument.relationshipsFor(partName)
			.getRelByType(
				this.generalDocument.documentRelationshipsNamespace().uri().concat(SLIDE_LAYOUT)
			);
		final SlideFragments slideLayoutFragments;
		if (null != rels && !rels.isEmpty() && this.slideLayoutFragmentsByName.containsKey(rels.get(0).target)) {
			slideLayoutFragments = this.slideLayoutFragmentsByName.get(rels.get(0).target);
		} else {
			slideLayoutFragments = new SlideFragments.Empty(
				new SlideTemplateFragments.Empty()
			);
		}
		try (final Reader reader = this.generalDocument.getPartReader(partName)) {
			final XMLEventReader eventReader =
				this.generalDocument.inputFactory().createXMLEventReader(reader);
			while (eventReader.hasNext()) {
				final XMLEvent e = eventReader.nextEvent();
				if (e.isEndElement() && SlideFragments.C_SLD.equals(e.asEndElement().getName().getLocalPart())) {
					break;
				}
				if (!e.isStartElement()) {
					continue;
				}
				if (SlideFragments.C_SLD.equals(e.asStartElement().getName().getLocalPart())) {
					final SlideFragments slideFragments = new SlideFragments.Default(
						e.asStartElement(),
						this.generalDocument.conditionalParameters(),
						this.generalDocument.eventFactory(),
						slideLayoutFragments
					);
					slideFragments.readWith(eventReader);
					return slideFragments;
				}
			}
		}
		return new SlideFragments.Empty(slideLayoutFragments);
	}

	private Map<String, SlideFragments> notesMasterFragments() throws IOException, XMLStreamException {
		final Map<String, SlideFragments> notesMasterFragments =
			new HashMap<>(this.presentationFragments.notesMasterNames().size());
		for (final String name : this.presentationFragments.notesMasterNames()) {
			notesMasterFragments.put(name, notesMasterFragmentsOf(name));
		}
		return notesMasterFragments;
	}

	private SlideFragments notesMasterFragmentsOf(final String partName) throws IOException, XMLStreamException {
		try (final Reader reader = this.generalDocument.getPartReader(partName)) {
			final XMLEventReader eventReader = this.generalDocument.inputFactory().createXMLEventReader(reader);
			while (eventReader.hasNext()) {
				final XMLEvent e = eventReader.nextEvent();
				if (e.isEndElement() && NotesMasterFragments.NOTES_MASTER.equals(e.asEndElement().getName().getLocalPart())) {
					break;
				}
				if (!e.isStartElement()) {
					continue;
				}
				if (NotesMasterFragments.NOTES_MASTER.equals(e.asStartElement().getName().getLocalPart())) {
					final NotesMasterFragments notesMasterFragments = new NotesMasterFragments(
						e.asStartElement(),
						this.generalDocument.conditionalParameters(),
						this.generalDocument.eventFactory()
					);
					notesMasterFragments.readWith(eventReader);
					return notesMasterFragments;
				}
			}
		}
		return new NotesMasterFragments.Empty();
	}

	private Map<String, SlideFragments> notesSlideFragments() throws IOException, XMLStreamException {
		final Map<String, SlideFragments> slideFragments = new HashMap<>(this.notesSlideNames.size());
		for (final String name : this.notesSlideNames) {
			slideFragments.put(name, notesSlideFragmentsOf(name));
		}
		return slideFragments;
	}

	private SlideFragments notesSlideFragmentsOf(final String partName) throws IOException, XMLStreamException {
		final List<Relationships.Rel> rels = this.generalDocument.relationshipsFor(partName)
			.getRelByType(
				this.generalDocument.documentRelationshipsNamespace().uri().concat(NOTES_MASTER)
			);
		final SlideFragments notesMasterFragments =
			null !=rels && !rels.isEmpty() && this.notesMasterFragmentsByName.containsKey(rels.get(0).target)
				? this.notesMasterFragmentsByName.get(rels.get(0).target)
				: new NotesMasterFragments.Empty();
		try (final Reader reader = this.generalDocument.getPartReader(partName)) {
			final XMLEventReader eventReader = this.generalDocument.inputFactory().createXMLEventReader(reader);
			while (eventReader.hasNext()) {
				final XMLEvent e = eventReader.nextEvent();
				if (e.isEndElement() && SlideFragments.C_SLD.equals(e.asEndElement().getName().getLocalPart())) {
					break;
				}
				if (!e.isStartElement()) {
					continue;
				}
				if (SlideFragments.C_SLD.equals(e.asStartElement().getName().getLocalPart())) {
					final SlideFragments slideFragments = new SlideFragments.Default(
						e.asStartElement(),
						this.generalDocument.conditionalParameters(),
						this.generalDocument.eventFactory(),
						notesMasterFragments
					);
					slideFragments.readWith(eventReader);
					return slideFragments;
				}
			}
		}
		return new SlideFragments.Empty(notesMasterFragments);
	}

	private Map<String, Set<String>> slideLayoutsBySlide() throws IOException, XMLStreamException {
		final Map<String, Set<String>> map = new HashMap<>();
		final String uri = this.generalDocument.documentRelationshipsNamespace().uri().concat(SLIDE_LAYOUT);
		for (final String name : this.presentationFragments.slideNames()) {
			map.put(name, relatedPartsFor(name, uri));
		}
		return map;
	}

	private Map<String, Set<String>> slideMastersBySlideLayout()  throws IOException, XMLStreamException {
		final Map<String, Set<String>> map = new HashMap<>();
		final String uri = this.generalDocument.documentRelationshipsNamespace().uri().concat(SLIDE_MASTER);
		for (final String name : this.slideLayoutNames) {
			map.put(name, relatedPartsFor(name, uri));
		}
		return map;
	}

	private Map<String, Set<String>> notesMastersByNotesSlide()  throws IOException, XMLStreamException {
		final Map<String, Set<String>> map = new HashMap<>();
		final String uri = this.generalDocument.documentRelationshipsNamespace().uri().concat(NOTES_MASTER);
		for (final String name : this.notesSlideNames) {
			map.put(name, relatedPartsFor(name, uri));
		}
		return map;
	}

	private Set<String> relatedPartsFor(final String name, final String uri) throws IOException, XMLStreamException {
		final Set<String> names;
		final List<Rel> rels = this.generalDocument.relationshipsFor(name).getRelByType(uri);
		if (null == rels || rels.isEmpty()) {
			names = Collections.emptySet();
		} else {
			names = rels.stream()
				.map(rel -> rel.target)
				.collect(Collectors.toSet());
		}
		return names;
	}

	private Map<String, String> slidesBy(final String relatedPart) throws IOException, XMLStreamException {
		final String namespaceUri = this.generalDocument.documentRelationshipsNamespace().uri();
		return this.generalDocument.relsByEntry(
			this.presentationFragments.slideNames(),
			namespaceUri.concat(relatedPart)
		);
	}

	@Override
	public boolean hasNextPart() {
		return this.entries.hasMoreElements();
	}

	@Override
	public Part nextPart() throws IOException, XMLStreamException {
		final ZipEntry entry = this.entries.nextElement();
		final String contentType = this.generalDocument.contentTypeFor(entry);

		relsNameMatcher.reset(entry.getName());
		if (isRelationshipsPart(contentType) && relsNameMatcher.matches() && this.generalDocument.conditionalParameters().getExtractExternalHyperlinks()) {
			return new RelationshipsPart(
					this.generalDocument,
					entry
			);
		}

		if (!isTranslatablePart(entry)) {
			if (isModifiablePart(contentType)) {
				return new ModifiablePart(this.generalDocument, entry, this.generalDocument.inputStreamFor(entry));
			}
			return new NonModifiablePart(this.generalDocument, entry);
		}

		if (isStyledTextPart(entry)) {
			if (isSlidablePart(entry.getName(), contentType)) {
				return new SlidablePart(
					this.generalDocument,
					entry,
					slideFragmentsFor(entry.getName(), contentType)
				);
			}
			final StyleDefinitions styleDefinitions = new StyleDefinitions.Empty();
			final StyleOptimisation styleOptimisation = styleOptimisationFor(entry, styleDefinitions);
			return new StyledTextPart(
				this.generalDocument,
				entry,
				styleDefinitions,
				styleOptimisation
			);
		}

		final ContentFilter contentFilter = new ContentFilter(
			this.generalDocument.conditionalParameters(),
			entry.getName()
		);
		ParseType parseType = getParseType(contentType);
		this.generalDocument.conditionalParameters().nFileType = parseType;

		contentFilter.setUpConfig(parseType);

		// Other configuration
		return new DefaultPart(this.generalDocument, entry, contentFilter);
	}

	private static boolean isRelationshipsPart(String contentType) {
		return PACKAGE_RELATIONSHIPS.equals(contentType);
	}

	private StyleOptimisation styleOptimisationFor(final ZipEntry entry, final StyleDefinitions styleDefinitions) throws IOException, XMLStreamException {
		final Namespace namespace = this.generalDocument.namespacesOf(entry).forPrefix(Namespace.PREFIX_A);
		if (null == namespace) {
			return new StyleOptimisation.Bypass();
		}
		return new StyleOptimisation.Default(
			new StyleOptimisation.Bypass(),
			this.generalDocument.conditionalParameters(),
			this.generalDocument.eventFactory(),
			new QName(namespace.uri(), ParagraphBlockProperties.PPR, namespace.prefix()),
			new QName(namespace.uri(), RunProperties.DEF_RPR, namespace.prefix()),
			Collections.emptyList(),
			styleDefinitions
		);
	}

    private ParseType getParseType(String contentType) {
        ParseType parseType;
		if (contentType.equals(CORE_PROPERTIES_TYPE)) {
			parseType = ParseType.MSWORDDOCPROPERTIES;
		}
		else if (contentType.equals(Powerpoint.COMMENTS_TYPE)) {
			parseType = ParseType.MSPOWERPOINTCOMMENTS;
		}
		else {
			throw new IllegalStateException("Unexpected content type " + contentType);
		}

		return parseType;
	}

	private boolean isTranslatablePart(final ZipEntry entry) throws IOException, XMLStreamException {
		final String type = this.generalDocument.contentTypeFor(entry);
		if (!entry.getName().endsWith(".xml")) return false;
        if (isExcluded(entry.getName(), type)) return false;
		if (isHidden(entry.getName(), type)) return false;
		if (this.generalDocument.conditionalParameters().getTranslateDocProperties() && type.equals(CORE_PROPERTIES_TYPE)) return true;
		if (this.generalDocument.conditionalParameters().getTranslateComments() && type.equals(Powerpoint.COMMENTS_TYPE)) return true;
		if (isStyledTextPart(entry)) return true;
		return false;
	}

	private static boolean isModifiablePart(String contentType) {
		// @todo add parts which have been hidden or excluded
		return Powerpoint.MAIN_DOCUMENT_TYPE.equals(contentType)
			|| Powerpoint.THEME_TYPE.equals(contentType);
	}

	@Override
	public boolean isStyledTextPart(final ZipEntry entry) {
		final String type = this.generalDocument.contentTypeFor(entry);
		if (type.equals(Drawing.DIAGRAM_TYPE)) return true;
		if (type.equals(Drawing.CHART_TYPE)) return true;
		if (isSlidablePart(entry.getName(), type)) return true;
		return false;
	}

	private boolean isSlidablePart(String entryName, String type) {
		return null != slideFragmentsFor(entryName, type);
	}

	private SlideFragments slideFragmentsFor(final String entryName, final String type) {
		// @todo fully exclude hidden, see net.sf.okapi.filters.openxml.PowerpointDocument.isHidden
		if (this.generalDocument.conditionalParameters().getTranslatePowerpointMasters()) {
			if (Powerpoint.SLIDE_MASTER_TYPE.equals(type) && this.slideMasterFragmentsByName.containsKey(entryName)) {
				// translating slide masters which are in use by slide layouts
				return this.slideMasterFragmentsByName.get(entryName);
			}
			if (Powerpoint.SLIDE_LAYOUT_TYPE.equals(type) && this.slideLayoutFragmentsByName.containsKey(entryName)) {
				// translating slide layouts which are in use by slides
				return this.slideLayoutFragmentsByName.get(entryName);
			}
		}
		if (Powerpoint.SLIDE_TYPE.equals(type) && this.slideFragmentsByName.containsKey(entryName)) {
			return this.slideFragmentsByName.get(entryName);
		}
		if (this.generalDocument.conditionalParameters().getTranslatePowerpointNotes()) {
			if (this.generalDocument.conditionalParameters().getTranslatePowerpointMasters()) {
				if (Powerpoint.NOTES_MASTER_TYPE.equals(type) && this.notesMasterFragmentsByName.containsKey(entryName)) {
					// translating notes masters which are in use by notes slides
					return this.notesMasterFragmentsByName.get(entryName);
				}
			}
			if (Powerpoint.NOTES_SLIDE_TYPE.equals(type) && this.notesSlideFragmentsByName.containsKey(entryName)) {
				return this.notesSlideFragmentsByName.get(entryName);
			}
		}
		return null;
	}

	/**
	 * @param entryName ZIP entry name
	 * @param contentType the entry's content type
	 * @return {@code true} if the entry is to be excluded due to
	 * {@link ConditionalParameters#getPowerpointIncludedSlideNumbersOnly()} and
	 * {@link ConditionalParameters#tsPowerpointIncludedSlideNumbers}
	 */
	private boolean isExcluded(final String entryName, final String contentType) {
		if (!this.generalDocument.conditionalParameters().getPowerpointIncludedSlideNumbersOnly()) {
			return false;
		}
		switch (contentType) {
			case Powerpoint.SLIDE_TYPE:
				return isExcludedSlide(entryName);
			case Powerpoint.SLIDE_LAYOUT_TYPE:
				return isExcludedSlideLayout(entryName);
			case Powerpoint.SLIDE_MASTER_TYPE:
				return isExcludedSlideMaster(entryName);
			case Powerpoint.COMMENTS_TYPE:
				return isExcludedComment(entryName);
			case Powerpoint.NOTES_SLIDE_TYPE:
				return isExcludedNotesSlide(entryName);
			case Powerpoint.NOTES_MASTER_TYPE:
				return isExcludedNotesMaster(entryName);
			case Drawing.CHART_TYPE:
				return isExcludedChart(entryName);
			case Drawing.DIAGRAM_TYPE:
				return isExcludedDiagramData(entryName);
			default:
				return false;
		}
	}

	/**
	 * @param entryName the entry name
	 * @return {@code true} if the given entry represents a slide that was not included using
	 * option {@link ConditionalParameters#tsPowerpointIncludedSlideNumbers}
	 */
	private boolean isExcludedSlide(final String entryName) {
		int slideIndex = this.presentationFragments.slideNames().indexOf(entryName);
		if (slideIndex == -1) {
			return false;
		}
		int slideNumber = slideIndex + 1; // human readable / 1-based slide numbers
		return !this.generalDocument.conditionalParameters().tsPowerpointIncludedSlideNumbers.contains(slideNumber);
	}

	private boolean isExcludedSlideLayout(final String entryName) {
		boolean excluded = false;
		for (final String slideName : this.slideLayoutsBySlide.keySet()) {
			if (this.slideLayoutsBySlide.get(slideName).contains(entryName)) {
				if (isExcludedSlide(slideName)) {
					excluded = true;
				} else {
					return false;
				}
			}
		}
		return excluded;
	}

	private boolean isExcludedSlideMaster(final String entryName) {
		boolean excluded = false;
		for (final String slideLayout : this.slideMastersBySlideLayout.keySet()) {
			if (this.slideMastersBySlideLayout.get(slideLayout).contains(entryName)) {
				if (isExcludedSlideLayout(slideLayout)) {
					excluded = true;
				} else {
					return false;
				}
			}
		}
		return excluded;
	}

	/**
	 * @param entryName the entry name
	 * @return {@code true} if the given entry represents a comment that is used on a slide that was
	 * not included using option {@link ConditionalParameters#tsPowerpointIncludedSlideNumbers}
	 */
	private boolean isExcludedComment(final String entryName) {
		if (!this.slidesByComment.containsKey(entryName)) {
			return false;
		}
		return isExcludedSlide(this.slidesByComment.get(entryName));
	}

	/**
	 * @param entryName the entry name
	 * @return {@code true} if the given entry represents a note that is used on a slide that was
	 * not included using option {@link ConditionalParameters#tsPowerpointIncludedSlideNumbers}
	 */
	private boolean isExcludedNotesSlide(final String entryName) {
		if (!this.slidesByNotesSlide.containsKey(entryName)) {
			return false;
		}
		return isExcludedSlide(this.slidesByNotesSlide.get(entryName));
	}

	private boolean isExcludedNotesMaster(final String entryName) {
		boolean excluded = false;
		for (final String notesSlide : this.notesMastersByNotesSlide.keySet()) {
			if (this.notesMastersByNotesSlide.get(notesSlide).contains(entryName)) {
				if (isExcludedNotesSlide(notesSlide)) {
					excluded = true;
				} else {
					return false;
				}
			}
		}
		return excluded;
	}

	/**
	 * @param entryName the entry name
	 * @return {@code true} if the given entry represents a chart that is used on a slide that was
	 * not included using option {@link ConditionalParameters#tsPowerpointIncludedSlideNumbers}
	 */
	private boolean isExcludedChart(final String entryName) {
		if (!this.slidesByChart.containsKey(entryName)) {
			return false;
		}
		return isExcludedSlide(this.slidesByChart.get(entryName));
	}

	/**
	 * "Diagram data" is used by SmartArt, for example.
	 *
	 * @param entryName the entry name
	 * @return {@code true} if the given entry represents a diagram that is used on a slide that was
	 * not included using option {@link ConditionalParameters#tsPowerpointIncludedSlideNumbers}
	 */
	private boolean isExcludedDiagramData(final String entryName) {
		if (!this.slidesByDiagramData.containsKey(entryName)) {
			return false;
		}
		return isExcludedSlide(this.slidesByDiagramData.get(entryName));
	}

	private boolean isHidden(final String entryName, final String contentType) {
		if (this.generalDocument.conditionalParameters().getTranslatePowerpointHidden()) {
			return false;
		}
		switch (contentType) {
			case Powerpoint.SLIDE_TYPE:
				return isHiddenSlide(entryName);
			case Powerpoint.SLIDE_LAYOUT_TYPE:
				return isHiddenSlideLayout(entryName);
			case Powerpoint.SLIDE_MASTER_TYPE:
				return isHiddenSlideMaster(entryName);
			case Powerpoint.COMMENTS_TYPE:
				return isHiddenComment(entryName);
			case Powerpoint.NOTES_SLIDE_TYPE:
				return isHiddenNotesSlide(entryName);
			case Powerpoint.NOTES_MASTER_TYPE:
				return isHiddenNotesMaster(entryName);
			case Drawing.CHART_TYPE:
				return isHiddenChart(entryName);
			case Drawing.DIAGRAM_TYPE:
				return isHiddenDiagramData(entryName);
			default:
				return false;
		}
	}

	private boolean isHiddenSlide(final String entryName) {
		return this.hiddenSlides.contains(entryName);
	}

	private boolean isHiddenSlideLayout(final String entryName) {
		boolean hidden = false;
		for (final String slideName : this.slideLayoutsBySlide.keySet()) {
			if (this.slideLayoutsBySlide.get(slideName).contains(entryName)) {
				if (isHiddenSlide(slideName)) {
					hidden = true;
				} else {
					return false;
				}
			}
		}
		return hidden;
	}

	private boolean isHiddenSlideMaster(final String entryName) {
		boolean hidden = false;
		for (final String slideLayout : this.slideMastersBySlideLayout.keySet()) {
			if (this.slideMastersBySlideLayout.get(slideLayout).contains(entryName)) {
				if (isHiddenSlideLayout(slideLayout)) {
					hidden = true;
				} else {
					return false;
				}
			}
		}
		return hidden;
	}

	private boolean isHiddenComment(final String entryName) {
		if (!this.slidesByComment.containsKey(entryName)) {
			return false;
		}
		return isHiddenSlide(this.slidesByComment.get(entryName));
	}

	private boolean isHiddenNotesSlide(final String entryName) {
		if (!this.slidesByNotesSlide.containsKey(entryName)) {
			return false;
		}
		return isHiddenSlide(this.slidesByNotesSlide.get(entryName));
	}

	private boolean isHiddenNotesMaster(final String entryName) {
		boolean hidden = false;
		for (final String notesSlide : this.notesMastersByNotesSlide.keySet()) {
			if (this.notesMastersByNotesSlide.get(notesSlide).contains(entryName)) {
				if (isHiddenNotesSlide(notesSlide)) {
					hidden = true;
				} else {
					return false;
				}
			}
		}
		return hidden;
	}

	private boolean isHiddenChart(final String entryName) {
		if (!this.slidesByChart.containsKey(entryName)) {
			return false;
		}
		return isHiddenSlide(this.slidesByChart.get(entryName));
	}

	private boolean isHiddenDiagramData(final String entryName) {
		if (!this.slidesByDiagramData.containsKey(entryName)) {
			return false;
		}
		return isHiddenSlide(this.slidesByDiagramData.get(entryName));
	}

	@Override
	public void close() throws IOException {
	}
}
