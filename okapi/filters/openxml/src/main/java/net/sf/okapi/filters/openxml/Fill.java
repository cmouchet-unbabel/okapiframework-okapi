/*
 * =============================================================================
 * Copyright (C) 2010-2021 by the Okapi Framework contributors
 * -----------------------------------------------------------------------------
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =============================================================================
 */
package net.sf.okapi.filters.openxml;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.util.LinkedList;
import java.util.List;

interface Fill {
    String NAME = "fill";
    PatternFill pattern();
    void readWith(final XMLEventReader reader) throws XMLStreamException;
    Markup asMarkup();

    final class Empty implements Fill {
        @Override
        public PatternFill pattern() {
            return new PatternFill.Empty();
        }

        @Override
        public void readWith(final XMLEventReader reader) throws XMLStreamException {
        }

        @Override
        public Markup asMarkup() {
            return new Markup.Empty();
        }
    }

    final class Default implements Fill {
        private static final String GRADIENT_FILL = "gradientFill";
        private final StartElement startElement;
        private List<XMLEvent> gradientFillEvents;
        private PatternFill patternFill;
        private EndElement endElement;

        Default(final StartElement startElement) {
            this.startElement = startElement;
        }

        @Override
        public PatternFill pattern() {
            if (null == this.patternFill) {
                this.patternFill = new PatternFill.Empty();
            }
            return this.patternFill;
        }

        @Override
        public void readWith(final XMLEventReader reader) throws XMLStreamException {
            while (reader.hasNext()) {
                final XMLEvent e = reader.nextEvent();
                if (e.isEndElement() && e.asEndElement().getName().equals(this.startElement.getName())) {
                    this.endElement = e.asEndElement();
                    break;
                }
                if (!e.isStartElement()) {
                    continue;
                }
                final StartElement se = e.asStartElement();
                if (GRADIENT_FILL.equals(se.getName().getLocalPart())) {
                    this.gradientFillEvents = eventsFor(se, reader);
                } else if (PatternFill.NAME.equals(se.getName().getLocalPart())) {
                    this.patternFill = new PatternFill.Default(se);
                    this.patternFill.readWith(reader);
                } else {
                    throw new IllegalStateException("Unsupported element found: ".concat(se.getName().getLocalPart()));
                }
            }
        }

        private static List<XMLEvent> eventsFor(final StartElement startElement, final XMLEventReader reader) throws XMLStreamException {
            final List<XMLEvent> events = new LinkedList<>();
            while (reader.hasNext()) {
                final XMLEvent e = reader.nextEvent();
                events.add(e);
                if (e.isEndElement() && e.asEndElement().getName().equals(startElement.getName())) {
                    break;
                }
            }
            return events;
        }

        @Override
        public Markup asMarkup() {
            if (null == this.endElement) {
                throw new IllegalStateException("The end element is not available");
            }
            final MarkupBuilder mb = new MarkupBuilder(new Markup.General(new LinkedList<>()));
            mb.add(this.startElement);
            if (null != this.gradientFillEvents) {
                mb.addAll(this.gradientFillEvents);

            }
            mb.add(pattern().asMarkup());
            mb.add(this.endElement);
            return mb.build();
        }
    }

}
