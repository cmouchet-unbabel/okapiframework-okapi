/*===========================================================================
  Copyright (C) 2016-2017 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
===========================================================================*/

package net.sf.okapi.filters.openxml;

import net.sf.okapi.common.Event;
import net.sf.okapi.common.encoder.EncoderManager;
import net.sf.okapi.common.exceptions.OkapiBadFilterInputException;
import net.sf.okapi.common.exceptions.OkapiUnexpectedRevisionException;
import net.sf.okapi.common.filters.IFilter;
import net.sf.okapi.filters.openxml.ContentTypes.Types.Common;
import net.sf.okapi.filters.openxml.ContentTypes.Types.Drawing;
import net.sf.okapi.filters.openxml.ContentTypes.Types.Excel;

import javax.xml.stream.XMLStreamException;
import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.zip.ZipEntry;

class ExcelDocument implements Document {
	private static final String EMPTY = "";
	private static final String REVISION_HEADERS = "/revisionHeaders";
	private static final String USER_NAMES = "/usernames";
	private static final String STYLES = "/styles";
	private static final String TABLE = "/table";
	private static final String PIVOT_TABLE = "/pivotTable";
	private static final String PIVOT_CACHE_DEFINITION = "/pivotCacheDefinition";
	private static final String COMMENT = "/comments";
	private static final String DRAWING = "/drawing";
	private static final String CHART = "/chart";
	private static final String DIAGRAM_DATA = "/diagramData";
	private static final String SHARED_STRINGS = "/sharedStrings";

	private final Document.General generalDocument;
	private final EncoderManager encoderManager;
	private final IFilter subfilter;
	private Map<Metadata, Set<String>> dispersedTranslationRawSourcesByMetadata;
	private Map<ZipEntry, Markup> postponedParts;
	private Cells cells;

	private Relationships workbookRelationships;
	private String revisionHeadersPartName;
	private String revisionUserNamesPartName;
	private String relationshipsPartNameForRevisionHeaders;
	private Set<String> revisionLogPartNames;
	private WorkbookFragments workbookFragments;
	private Enumeration<? extends ZipEntry> entries;
	private StyleDefinitions styleDefinitions;

	private Map<String, Set<String>> tablesByWorksheet;
	private Map<String, Set<String>> pivotTablesByWorksheet;
	private Map<String, Set<String>> pivotCacheDefinitionsByPivotTable;
	private Map<String, String> worksheetsByComment;
	private Map<String, String> worksheetsByDrawing;
	private Map<String, String> drawingsByChart;
	private Map<String, String> drawingsByDiagramData;

	ExcelDocument(final General generalDocument, EncoderManager encoderManager, IFilter subfilter) {
		this.generalDocument = generalDocument;
		this.encoderManager = encoderManager;
		this.subfilter = subfilter;
	}

	@Override
	public Event open() throws IOException, XMLStreamException {
		this.dispersedTranslationRawSourcesByMetadata = new HashMap<>();
		this.postponedParts = new LinkedHashMap<>();
		this.cells = new Cells.Default(generalDocument.eventFactory(), new LinkedList<>());
		this.workbookRelationships = this.generalDocument.relationshipsFor(this.generalDocument.mainPartName());
		loadAndCheckRevisions();
		this.workbookFragments = workbookFragments();
		loadLegacyConfigurations();
		this.styleDefinitions = styleDefinitions();
		this.tablesByWorksheet = tablesByWorksheet();
		this.pivotTablesByWorksheet = pivotTablesByWorksheet();
		this.pivotCacheDefinitionsByPivotTable = pivotCacheDefinitionsByPivotTable();
		this.worksheetsByComment = worksheetsBy(COMMENT);
		this.worksheetsByDrawing = worksheetsBy(DRAWING);
		this.drawingsByChart = drawingsBy(CHART);
		this.drawingsByDiagramData = drawingsBy(DIAGRAM_DATA);
		this.entries = entries();
		return this.generalDocument.startDocumentEvent();
	}

	private void loadAndCheckRevisions() throws XMLStreamException, IOException {
		this.revisionHeadersPartName = revisionHeadersPartName();
		this.revisionUserNamesPartName = revisionUserNamesPartName();
		this.revisionLogPartNames = new HashSet<>();
		if (!this.revisionHeadersPartName.isEmpty()) {
			this.relationshipsPartNameForRevisionHeaders = this.generalDocument.relationshipsPartNameFor(this.revisionHeadersPartName);
			final String revisionHeadersNamespaceUri = this.generalDocument.documentRelationshipsNamespace().uri().concat(REVISION_HEADERS);
			final List<Relationships.Rel> revisionHeadersRels = this.workbookRelationships.getRelByType(
				revisionHeadersNamespaceUri
			);
			if (1 != revisionHeadersRels.size()) {
				throw new OkapiBadFilterInputException(
					String.format("%s: %s", Relationships.UNEXPECTED_NUMBER_OF_RELATIONSHIPS, revisionHeadersNamespaceUri)
				);
			}
			final Relationships revisionHeadersRelationships = this.generalDocument.relationshipsFor(this.revisionHeadersPartName);
			for (final RevisionHeaderFragments revisionHeaderFragments : revisionHeadersFragments()) {
				final String revisionLogPartName = revisionHeadersRelationships.getRelById(revisionHeaderFragments.revisionLogId()).target;
				final RevisionLogFragments revisionLogFragments = revisionLogFragmentsFor(revisionLogPartName);
				if (!this.generalDocument.conditionalParameters().getAutomaticallyAcceptRevisions()
					&& !revisionHeaderFragments.reviewed(revisionLogFragments.revisionIds())) {
					throw new OkapiUnexpectedRevisionException();
				}
				this.revisionLogPartNames.add(revisionLogPartName);
			}
		} else {
			this.relationshipsPartNameForRevisionHeaders = EMPTY;
		}
	}

	private String revisionHeadersPartName() throws XMLStreamException, IOException {
		return partNameFor(REVISION_HEADERS);
	}

	private String revisionUserNamesPartName() throws XMLStreamException, IOException {
		return partNameFor(USER_NAMES);
	}

	private String partNameFor(final String type) throws XMLStreamException, IOException {
		final String partPath = this.generalDocument.relationshipTargetFor(
			this.generalDocument.documentRelationshipsNamespace().uri().concat(type)
		);
		return null == partPath
			? EMPTY
			: partPath;
	}

	private RevisionHeadersFragments revisionHeadersFragments() throws IOException, XMLStreamException {
		final RevisionHeadersFragments rhf = new RevisionHeadersFragments.Default();
		try (final Reader reader = this.generalDocument.getPartReader(this.revisionHeadersPartName)) {
			rhf.readWith(this.generalDocument.inputFactory().createXMLEventReader(reader));
		}
		return rhf;
	}

	private RevisionLogFragments revisionLogFragmentsFor(final String partName) throws IOException, XMLStreamException {
		final RevisionLogFragments rlf = new RevisionLogFragments.Default();
		try (final Reader reader = this.generalDocument.getPartReader(partName)) {
			rlf.readWith(this.generalDocument.inputFactory().createXMLEventReader(reader));
		}
		return rlf;
	}

	private void loadLegacyConfigurations() {
		if (this.generalDocument.conditionalParameters().getTranslateExcelExcludeColumns()) {
			this.generalDocument.conditionalParameters().worksheetConfigurations().addFrom(
				new ExcelExcludedColumnsWorksheetConfigurationsInput(
					this.workbookFragments,
					this.generalDocument.conditionalParameters().tsExcelExcludedColumns
				)
			);
		}
	}

	private WorkbookFragments workbookFragments() throws IOException, XMLStreamException {
		final WorkbookFragments wf = new WorkbookFragments.Default(
			this.generalDocument.conditionalParameters(),
			this.workbookRelationships
		);
		try (final Reader reader = this.generalDocument.getPartReader(this.generalDocument.mainPartName())) {
			wf.readWith(this.generalDocument.inputFactory().createXMLEventReader(reader));
		}
		return wf;
	}

	/**
	 * Do additional reordering of the entries for XLSX files to make
	 * sure that worksheets are parsed in order, followed by the shared
	 * strings table.
	 * @return the sorted enum of ZipEntry
	 * @throws IOException if any error is encountered while reading the stream
	 * @throws XMLStreamException if any error is encountered while parsing the XML
	 */
	private Enumeration<? extends ZipEntry> entries() throws IOException, XMLStreamException {
		final List<? extends ZipEntry> entryList = Collections.list(this.generalDocument.entries());
		Iterator<? extends  ZipEntry> iterator = entryList.iterator();
		while (iterator.hasNext()) {
			final String name = iterator.next().getName();
			if (name.equals(this.revisionHeadersPartName)
				|| name.equals(this.revisionUserNamesPartName)
				|| name.equals(this.relationshipsPartNameForRevisionHeaders)
				|| this.revisionLogPartNames.contains(name)) {
				iterator.remove();
			}
		}
		entryList.sort(new ZipEntryComparator(reorderedPartNames()));
		return Collections.enumeration(entryList);
	}

	private List<String> reorderedPartNames() throws IOException, XMLStreamException {
		final List<String> names = new ArrayList<>();
		names.addAll(
			this.tablesByWorksheet.values().stream()
				.flatMap(strings -> strings.stream()).collect(Collectors.toSet())
		);
		names.addAll(
			this.pivotTablesByWorksheet.values().stream()
				.flatMap(strings -> strings.stream()).collect(Collectors.toSet())
		);
		names.addAll(
			this.pivotCacheDefinitionsByPivotTable.values().stream()
				.flatMap(strings -> strings.stream()).collect(Collectors.toSet())
		);
		names.addAll(this.workbookFragments.worksheetPartNames());
		final String sharedStringsName = sharedStringsName();
		if (!EMPTY.equals(sharedStringsName)) {
			names.add(sharedStringsName);
		}
		// @todo: handle definedNames > name@definedName as structured references, formulas, etc
		names.add(this.generalDocument.mainPartName());
		final String stylesName = stylesName();
		if (!EMPTY.equals(stylesName)) {
			names.add(stylesName);
		}
		return names;
	}

	/**
	 * Parse relationship information to find the shared strings table.
	 * @return {@code ExcelDocument#EMPTY} if there is no the shared strings relationship
	 *         or a found shared strings entry name otherwise.
	 * @throws IOException
	 * @throws XMLStreamException
	 */
	private String sharedStringsName() throws IOException, XMLStreamException {
		final String sharedStringsNamespaceUri = this.generalDocument.documentRelationshipsNamespace()
			.uri().concat(SHARED_STRINGS);
		List<Relationships.Rel> r = this.workbookRelationships.getRelByType(sharedStringsNamespaceUri);
		if (r == null) {
			return EMPTY;
		}
		if (r.size() != 1) {
			throw new OkapiBadFilterInputException(
				String.format("%s: %s", Relationships.UNEXPECTED_NUMBER_OF_RELATIONSHIPS, sharedStringsNamespaceUri)
			);
		}
		return r.get(0).target;
	}

	private String stylesName() throws IOException, XMLStreamException {
		return partNameFor(STYLES);
	}

	private StyleDefinitions styleDefinitions() throws IOException, XMLStreamException {
		final String partPath = stylesName();
		if (null == partPath) {
			return new StyleDefinitions.Empty();
		}
		try (final Reader reader = this.generalDocument.getPartReader(partPath)) {
			final StyleDefinitions styleDefinitions = new ExcelStyleDefinitions();
			styleDefinitions.readWith(
				new ExcelStyleDefinitionsReader(
					this.generalDocument.conditionalParameters(),
					this.generalDocument.eventFactory(),
					this.generalDocument.inputFactory().createXMLEventReader(reader)
				)
			);
			return styleDefinitions;
		}
	}

	private Map<String, Set<String>> tablesByWorksheet() throws IOException, XMLStreamException {
		final Map<String, Set<String>> map = new HashMap<>();
		final String uri = this.generalDocument.documentRelationshipsNamespace().uri().concat(TABLE);
		for (final String name : this.workbookFragments.worksheetPartNames()) {
			map.put(name, relatedPartsFor(name, uri));
		}
		return map;
	}

	private Map<String, Set<String>> pivotTablesByWorksheet() throws IOException, XMLStreamException {
		final Map<String, Set<String>> map = new HashMap<>();
		final String uri = this.generalDocument.documentRelationshipsNamespace().uri().concat(PIVOT_TABLE);
		for (final String name : this.workbookFragments.worksheetPartNames()) {
			map.put(name, relatedPartsFor(name, uri));
		}
		return map;
	}

	private Map<String, Set<String>> pivotCacheDefinitionsByPivotTable() throws IOException, XMLStreamException {
		final Map<String, Set<String>> map = new HashMap<>();
		final String uri = this.generalDocument.documentRelationshipsNamespace().uri().concat(PIVOT_CACHE_DEFINITION);
		final Set<String> pivotTableNames = this.pivotTablesByWorksheet.values().stream()
				.flatMap(strings -> strings.stream()).collect(Collectors.toSet());
		for (final String name : pivotTableNames) {
			map.put(name, relatedPartsFor(name, uri));
		}
		return map;
	}

	private Set<String> relatedPartsFor(final String name, final String uri) throws IOException, XMLStreamException {
		final Set<String> names;
		final List<Relationships.Rel> rels = this.generalDocument.relationshipsFor(name).getRelByType(uri);
		if (null == rels || rels.isEmpty()) {
			names = Collections.emptySet();
		} else {
			names = rels.stream()
				.map(rel -> rel.target)
				.collect(Collectors.toSet());
		}
		return names;
	}

	private Map<String, String> worksheetsBy(final String relatedPart) throws IOException, XMLStreamException {
		final String namespaceUri = this.generalDocument.documentRelationshipsNamespace().uri();
		return this.generalDocument.relsByEntry(
			this.workbookFragments.worksheetPartNames(),
			namespaceUri.concat(relatedPart)
		);
	}

	private Map<String, String> drawingsBy(final String relatedPart) throws IOException, XMLStreamException {
		final String namespaceUri = this.generalDocument.documentRelationshipsNamespace().uri();
		return this.generalDocument.relsByEntry(
			new ArrayList<>(this.worksheetsByDrawing.keySet()),
			namespaceUri.concat(relatedPart)
		);
	}

	@Override
	public boolean hasNextPart() {
		return this.entries.hasMoreElements() || !this.postponedParts.isEmpty();
	}

	@Override
	public Part nextPart() throws IOException, XMLStreamException {
		if (!this.entries.hasMoreElements()) {
			return nextPostponedPart();
		}
		final ZipEntry entry = this.entries.nextElement();
		final String contentType = this.generalDocument.contentTypeFor(entry);

		// find a part based on content type
		if (!isTranslatablePart(entry.getName(), contentType)) {
			if (isModifiablePart(contentType)) {
				switch (contentType) {
					case Excel.TABLE_TYPE: {
						final String worksheetPartName = this.tablesByWorksheet.entrySet().stream()
							.filter(e -> e.getValue().contains(entry.getName()))
							.map(e -> e.getKey())
							.findFirst()
							.orElseThrow(() -> new IllegalStateException("Unlinked table part found: ".concat(entry.getName())));
						final String worksheetName = this.workbookFragments.localisedWorksheetNameFor(worksheetPartName);
						final Table table = new Table.Default(this.generalDocument.eventFactory(), worksheetName);
						try (final Reader reader = this.generalDocument.getPartReader(entry.getName())) {
							table.readWith(this.generalDocument.inputFactory().createXMLEventReader(reader));
						}
						if (!isTableHidden(entry.getName())) {
							this.dispersedTranslationRawSourcesByMetadata.put(
								new Metadata(
									worksheetName,
									table.cellReferencesRange(),
									table.name()
								),
								table.columnNames()
							);
						}
						this.postponedParts.put(entry, table.asMarkup());
						break;
					}
					case Excel.PIVOT_TABLE_TYPE:
						final PivotTable pivotTable = new PivotTable.Default(this.generalDocument.eventFactory());
						try (final Reader reader = this.generalDocument.getPartReader(entry.getName())) {
							pivotTable.readWith(this.generalDocument.inputFactory().createXMLEventReader(reader));
						}
						if (!isPivotTableHidden(entry.getName())) {
							final String worksheetPartName = this.pivotTablesByWorksheet.entrySet().stream()
								.filter(e -> e.getValue().contains(entry.getName()))
								.map(e -> e.getKey())
								.findFirst()
								.orElseThrow(() -> new IllegalStateException(("Unlinked pivot table part found: ").concat(entry.getName())));
							this.dispersedTranslationRawSourcesByMetadata.put(
								new Metadata(
									this.workbookFragments.localisedWorksheetNameFor(worksheetPartName),
									pivotTable.cellReferencesRange(),
									pivotTable.name()
								),
								pivotTable.dataFieldNames()
							);
						}
						this.postponedParts.put(entry, pivotTable.asMarkup());
						break;
					case Excel.PIVOT_CACHE_DEFINITION_TYPE:
						final PivotCacheDefinition pcd = new PivotCacheDefinition.Default(
							this.generalDocument.eventFactory()
						);
						try (final Reader reader = this.generalDocument.getPartReader(entry.getName())) {
							pcd.readWith(this.generalDocument.inputFactory().createXMLEventReader(reader));
						}
						if (!isPivotCacheDefinitionHidden(entry.getName())) {
							if (!pcd.sheetNameSource().isEmpty() && !pcd.cellReferencesRangeSource().toString().isEmpty()) {
								this.dispersedTranslationRawSourcesByMetadata.put(
									new Metadata(
										pcd.sheetNameSource(),
										pcd.cellReferencesRangeSource(),
										pcd.sheetNameSource()
									),
									pcd.cacheFieldNames()
								);
							}
						}
						this.postponedParts.put(entry, pcd.asMarkup());
						break;
					case Excel.WORKSHEET_TYPE:
						final Worksheet worksheet = new Worksheet.Default(
							this.generalDocument.conditionalParameters(),
							this.generalDocument.eventFactory(),
							this.workbookFragments.date1904(),
							this.cells,
							this.styleDefinitions,
							this.workbookFragments.localisedWorksheetNameFor(entry.getName()),
							new WorksheetFragments.Default(
								this.generalDocument.conditionalParameters().getTranslateExcelHidden(),
								isWorksheetHidden(entry.getName())
							)
						);
						try (final Reader reader = this.generalDocument.getPartReader(entry.getName())) {
							worksheet.readWith(this.generalDocument.inputFactory().createXMLEventReader(reader));
						}
						this.postponedParts.put(entry, worksheet.asMarkup());
						break;
					case Excel.STYLES_TYPE:
						return new MarkupModifiablePart(
							this.generalDocument,
							entry,
							this.styleDefinitions.asMarkup()
						);
					default:
						throw new IllegalStateException("Unsupported modifiable content type: ".concat(contentType));
				}
				return nextPart();
			}
			return new NonModifiablePart(this.generalDocument, entry);
		}

		if (isStyledTextPart(entry)) {
			// @todo: optimise direct and style based formatting
			final StyleDefinitions styleDefinitions = new StyleDefinitions.Empty();
			final StyleOptimisation styleOptimisation = new StyleOptimisation.Bypass();

			if (Excel.SHARED_STRINGS_TYPE.equals(contentType)) {
				return new SharedStringsPart(
					this.generalDocument,
					entry,
					styleDefinitions,
					styleOptimisation,
					this.encoderManager,
					this.subfilter,
					this.dispersedTranslationRawSourcesByMetadata,
					this.cells
				);
			}
			if (Excel.COMMENT_TYPE.equals(contentType)) {
				return new ExcelCommentPart(this.generalDocument, entry, styleDefinitions, styleOptimisation);
			}
			return new StyledTextPart(
				this.generalDocument,
				entry,
				styleDefinitions,
				styleOptimisation
			);
		}
		if (Excel.MAIN_DOCUMENT_TYPE.equals(contentType)
			|| Excel.MACRO_ENABLED_MAIN_DOCUMENT_TYPE.equals(contentType)) {
			return new WorkbookPart(this.generalDocument, entry, this.workbookFragments);
		}
		ParseType parseType = ParseType.MSEXCEL;
		if (Common.CORE_PROPERTIES_TYPE.equals(contentType)) {
			parseType = ParseType.MSWORDDOCPROPERTIES;
		}
		final ContentFilter contentFilter = new ContentFilter(
			this.generalDocument.conditionalParameters(),
			entry.getName()
		);
		contentFilter.setUpConfig(parseType);
		return new DefaultPart(this.generalDocument, entry, contentFilter);
	}

	private Part nextPostponedPart() {
		final Iterator<Map.Entry<ZipEntry, Markup>> iterator = postponedParts.entrySet().iterator();
		final Map.Entry<ZipEntry, Markup> mapEntry = iterator.next();
		iterator.remove();
		return new MarkupModifiablePart(
			this.generalDocument,
			mapEntry.getKey(),
			mapEntry.getValue()
		);
	}

	private boolean isTranslatablePart(String entryName, String contentType) {
		if (!entryName.endsWith(".xml")) {
			return false;
		}
		if (isHidden(entryName, contentType)) {
			return false;
		}
		switch (contentType) {
			case Excel.MAIN_DOCUMENT_TYPE:
			case Excel.MACRO_ENABLED_MAIN_DOCUMENT_TYPE:
				return this.generalDocument.conditionalParameters().getTranslateExcelSheetNames();
			case Common.CORE_PROPERTIES_TYPE:
				return this.generalDocument.conditionalParameters().getTranslateDocProperties();
			case Excel.SHARED_STRINGS_TYPE:
				return true;
			case Excel.COMMENT_TYPE:
				return this.generalDocument.conditionalParameters().getTranslateComments();
			case Excel.DRAWINGS_TYPE:
				return this.generalDocument.conditionalParameters().getTranslateExcelDrawings();
			case Drawing.CHART_TYPE:
				return true;
			case Drawing.DIAGRAM_TYPE:
				return this.generalDocument.conditionalParameters().getTranslateExcelDiagramData();
			default:
				return false;
		}
	}

	private boolean isModifiablePart(String contentType) {
		return Excel.STYLES_TYPE.equals(contentType)
			|| Excel.WORKSHEET_TYPE.equals(contentType)
			|| Excel.TABLE_TYPE.equals(contentType)
			|| Excel.PIVOT_TABLE_TYPE.equals(contentType)
			|| Excel.PIVOT_CACHE_DEFINITION_TYPE.equals(contentType);
	}

	@Override
	public boolean isStyledTextPart(final ZipEntry entry) {
		final String type = this.generalDocument.contentTypeFor(entry);
		return Excel.SHARED_STRINGS_TYPE.equals(type)
			|| Excel.COMMENT_TYPE.equals(type)
			|| Excel.DRAWINGS_TYPE.equals(type)
			|| Drawing.CHART_TYPE.equals(type)
			|| Drawing.DIAGRAM_TYPE.equals(type);
	}

	private boolean isHidden(String entryName, String contentType) {
	    switch (contentType) {
	        case Excel.COMMENT_TYPE:
                return isCommentHidden(entryName);
	        case Excel.DRAWINGS_TYPE:
                return isDrawingHidden(entryName);
	        case Drawing.CHART_TYPE:
                return isChartHidden(entryName);
	        case Drawing.DIAGRAM_TYPE:
                return isDiagramDataHidden(entryName);
            default:
                return false;
        }
	}

	private boolean isWorksheetHidden(final String entryName) {
		return this.workbookFragments.worksheetPartNameHiddenFor(entryName);
	}

	private boolean isTableHidden(final String entryName) {
		for (final String worksheetName : this.tablesByWorksheet.keySet()) {
			if (this.tablesByWorksheet.get(worksheetName).contains(entryName)) {
				return isWorksheetHidden(worksheetName);
			}
		}
		return false;
	}

	private boolean isPivotTableHidden(final String entryName) {
		for (final String worksheetName : this.pivotTablesByWorksheet.keySet()) {
			if (this.pivotTablesByWorksheet.get(worksheetName).contains(entryName)) {
				return isWorksheetHidden(worksheetName);
			}
		}
		return false;
	}

	private boolean isPivotCacheDefinitionHidden(final String entryName) {
		for (final String pivotTableName : this.pivotCacheDefinitionsByPivotTable.keySet()) {
			if (this.pivotCacheDefinitionsByPivotTable.get(pivotTableName).contains(entryName)) {
				return isPivotTableHidden(pivotTableName);
			}
		}
		return false;
	}

	private boolean isCommentHidden(String entryName) {
		if (!this.worksheetsByComment.containsKey(entryName)) {
			return false;
		}
		return isWorksheetHidden(this.worksheetsByComment.get(entryName));
	}

	private boolean isDrawingHidden(String entryName) {
		if (!this.worksheetsByDrawing.containsKey(entryName)) {
			return false;
		}
		return isWorksheetHidden(this.worksheetsByDrawing.get(entryName));
	}

	private boolean isChartHidden(String entryName) {
		if (!this.drawingsByChart.containsKey(entryName)) {
			return false;
		}
		return isDrawingHidden(this.drawingsByChart.get(entryName));
	}

	private boolean isDiagramDataHidden(String entryName) {
		if (!this.drawingsByDiagramData.containsKey(entryName)) {
			return false;
		}
		return isDrawingHidden(this.drawingsByDiagramData.get(entryName));
	}

	@Override
	public void close() throws IOException {
	}
}
