/*
 * =============================================================================
 * Copyright (C) 2010-2021 by the Okapi Framework contributors
 * -----------------------------------------------------------------------------
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =============================================================================
 */
package net.sf.okapi.filters.openxml;

import javax.xml.stream.XMLEventFactory;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.util.LinkedList;
import java.util.List;

interface DifferentialFormat {
    String NAME = "dxf";
    NumberFormat numberFormat();
    Font font();
    Fill fill();
    CellAlignment alignment();
    CellProtection protection();
    void readWith(final XMLEventReader reader) throws XMLStreamException;
    Markup asMarkup();

    final class Empty implements DifferentialFormat {
        @Override
        public NumberFormat numberFormat() {
            return NumberFormat.BuiltIn.GENERAL;
        }

        @Override
        public Font font() {
            return new Font.Empty();
        }

        @Override
        public Fill fill() {
            return new Fill.Empty();
        }

        @Override
        public CellAlignment alignment() {
            return new CellAlignment.Empty();
        }

        @Override
        public CellProtection protection() {
            return new CellProtection.Empty();
        }

        @Override
        public void readWith(final XMLEventReader reader) throws XMLStreamException {
        }

        @Override
        public Markup asMarkup() {
            return new Markup.Empty();
        }
    }

    final class Default implements DifferentialFormat {
        private static final String BORDER = "border";
        private final ConditionalParameters conditionalParameters;
        private final XMLEventFactory eventFactory;
        private final StartElement startElement;
        private NumberFormat numberFormat;
        private Font font;
        private Fill fill;
        private List<XMLEvent> borderEvents;
        private CellAlignment alignment;
        private CellProtection protection;
        private EndElement endElement;

        Default(
            final ConditionalParameters conditionalParameters,
            final XMLEventFactory eventFactory,
            final StartElement startElement
        ) {
            this.conditionalParameters = conditionalParameters;
            this.eventFactory = eventFactory;
            this.startElement = startElement;
        }

        @Override
        public NumberFormat numberFormat() {
            if (null == this.numberFormat) {
                this.numberFormat = NumberFormat.BuiltIn.GENERAL;
            }
            return this.numberFormat;
        }

        @Override
        public Font font() {
            if (null == this.font) {
                this.font = new Font.Empty();
            }
            return this.font;
        }

        @Override
        public Fill fill() {
            if (null == this.fill) {
                this.fill = new Fill.Empty();
            }
            return this.fill;
        }

        @Override
        public CellAlignment alignment() {
            if (null == this.alignment) {
                this.alignment = new CellAlignment.Empty();
            }
            return this.alignment;
        }

        @Override
        public CellProtection protection() {
            if (null == this.protection) {
                this.protection = new CellProtection.Empty();
            }
            return this.protection;
        }

        @Override
        public void readWith(final XMLEventReader reader) throws XMLStreamException {
            while (reader.hasNext()) {
                final XMLEvent e = reader.nextEvent();
                if (e.isEndElement() && e.asEndElement().getName().equals(this.startElement.getName())) {
                    this.endElement = e.asEndElement();
                    break;
                }
                if (!e.isStartElement()) {
                    continue;
                }
                final StartElement se = e.asStartElement();
                if (Font.NAME.equals(se.getName().getLocalPart())) {
                    this.font = new Font.Default(
                        this.conditionalParameters,
                        this.eventFactory,
                        se
                    );
                    this.font.readWith(reader);
                    continue;
                }
                if (NumberFormat.NAME.equals(se.getName().getLocalPart())) {
                    this.numberFormat = new NumberFormat.Explicit(se);
                    this.numberFormat.readWith(reader);
                    continue;
                }
                if (Fill.NAME.equals(se.getName().getLocalPart())) {
                    this.fill = new Fill.Default(se);
                    this.fill.readWith(reader);
                    continue;
                }
                if (CellAlignment.NAME.equals(se.getName().getLocalPart())) {
                    this.alignment = new CellAlignment.Default(this.eventFactory, se);
                    this.alignment.readWith(reader);
                    continue;
                }
                if (BORDER.equals(se.getName().getLocalPart())) {
                    this.borderEvents = eventsFor(se, reader);
                    continue;
                }
                if (CellProtection.NAME.equals(se.getName().getLocalPart())) {
                    this.protection = new CellProtection.Default(se);
                    this.protection.readWith(reader);
                }
            }
        }

        private static List<XMLEvent> eventsFor(final StartElement startElement, final XMLEventReader reader) throws XMLStreamException {
            final List<XMLEvent> events = new LinkedList<>();
            while (reader.hasNext()) {
                final XMLEvent e = reader.nextEvent();
                events.add(e);
                if (e.isEndElement() && e.asEndElement().getName().equals(startElement.getName())) {
                    break;
                }
            }
            return events;
        }

        @Override
        public Markup asMarkup() {
            if (null == this.endElement) {
                throw new IllegalStateException("The end element is not available");
            }
            final MarkupBuilder mb = new MarkupBuilder(new Markup.General(new LinkedList<>()));
            mb.add(this.startElement);
            mb.add(font().asMarkup());
            mb.add(numberFormat().asMarkup());
            mb.add(fill().asMarkup());
            mb.add(alignment().asMarkup());
            if (null != this.borderEvents) {
                mb.addAll(this.borderEvents);
            }
            mb.add(protection().asMarkup());
            mb.add(this.endElement);
            return mb.build();
        }
    }

    final class Combined implements DifferentialFormat {
        private final NumberFormat numberFormat;
        private final Font font;
        private final Fill fill;
        private final CellAlignment alignment;
        private final CellProtection protection;

        Combined(
            final NumberFormat numberFormat,
            final Font font,
            final Fill fill,
            final CellAlignment alignment,
            final CellProtection protection
        ) {
            this.numberFormat = numberFormat;
            this.font = font;
            this.fill = fill;
            this.alignment = alignment;
            this.protection = protection;
        }

        @Override
        public NumberFormat numberFormat() {
            return this.numberFormat;
        }

        @Override
        public Font font() {
            return this.font;
        }

        @Override
        public Fill fill() {
            return this.fill;
        }

        @Override
        public CellAlignment alignment() {
            return this.alignment;
        }

        @Override
        public CellProtection protection() {
            return this.protection;
        }

        @Override
        public void readWith(final XMLEventReader reader) throws XMLStreamException {
        }

        @Override
        public Markup asMarkup() {
            throw new UnsupportedOperationException();
        }
    }
}
