/*
 * =============================================================================
 * Copyright (C) 2010-2021 by the Okapi Framework contributors
 * -----------------------------------------------------------------------------
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =============================================================================
 */
package net.sf.okapi.filters.openxml;

import javax.xml.stream.XMLEventFactory;
import java.util.Collections;
import java.util.List;
import java.util.ListIterator;
import java.util.stream.Collectors;

interface Cells {
    void add(final Cell cell);
    Cells of(final CellType type);
    ListIterator<Cell> iterator();
    boolean worksheetStartsAt(final ListIterator<Cell> iterator);
    boolean rowStartsAt(final ListIterator<Cell> iterator);

    class Default implements Cells {
        private final XMLEventFactory eventFactory;
        private final List<Cell> items;
        private int sharedStringIndex;

        Default(final XMLEventFactory eventFactory, final List<Cell> items) {
            this.eventFactory = eventFactory;
            this.items = items;
        }

        @Override
        public void add(final Cell cell) {
            this.items.add(cell);
            if (CellType.SHARED_STRING == cell.type() || CellType.INLINE_STRING == cell.type()) {
                cell.value().update(
                    this.eventFactory.createCharacters(String.valueOf(this.sharedStringIndex))
                );
                this.sharedStringIndex++;
            }
        }

        @Override
        public Cells of(final CellType type) {
            return new Default(
                this.eventFactory,
                this.items.stream()
                    .filter(c -> c.type() == type)
                    .collect(Collectors.toList())
            );
        }

        @Override
        public ListIterator<Cell> iterator() {
            return this.items.listIterator();
        }

        @Override
        public boolean worksheetStartsAt(final ListIterator<Cell> iterator) {
            if (!iterator.hasPrevious()) {
                // at the start of the list
                return true;
            }
            final Cell current = iterator.previous();
            if (!iterator.hasPrevious()) {
                // at the start of the list
                iterator.next(); // restore the position
                return true;
            }
            final Cell previous = iterator.previous();
            iterator.next(); // restore the position
            iterator.next();
            return !previous.worksheetName().equals(current.worksheetName());
        }

        @Override
        public boolean rowStartsAt(final ListIterator<Cell> iterator) {
            if (!iterator.hasPrevious()) {
                // at the start of the list
                return true;
            }
            final Cell current = iterator.previous();
            if (!iterator.hasPrevious()) {
                // at the start of the list
                iterator.next(); // restore the position
                return true;
            }
            final Cell previous = iterator.previous();
            iterator.next(); // restore the position
            iterator.next();
            return previous.cellReferencesRange().first().row() != current.cellReferencesRange().first().row();
        }
    }
}
