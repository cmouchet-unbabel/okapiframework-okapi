/*
 * =============================================================================
 * Copyright (C) 2010-2021 by the Okapi Framework contributors
 * -----------------------------------------------------------------------------
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =============================================================================
 */
package net.sf.okapi.filters.openxml;

import java.util.Collections;
import java.util.Set;
import java.util.regex.Pattern;

/**
 * @see ConditionalParameters#tsExcelExcludedColumns for more information
 */
@Deprecated
final class ExcelExcludedColumnWorksheetConfiguration implements WorksheetConfiguration {
    private static final String DEFAULT_NAME_PATTERN = ".*";

    private final WorkbookFragments workbookFragments;
    private final String excludedColumnConfiguration;
    private WorksheetConfiguration worksheetConfiguration;
    private boolean read;

    ExcelExcludedColumnWorksheetConfiguration(final WorkbookFragments workbookFragments, final String excludedColumnConfiguration) {
        this.workbookFragments = workbookFragments;
        this.excludedColumnConfiguration = excludedColumnConfiguration;
    }

    @Override
    public boolean matches(final String worksheetName) {
        if (!this.read) {
            fromExcludedColumn();
        }
        return this.worksheetConfiguration.matches(worksheetName);
    }

    @Override
    public Set<Integer> excludedRows() {
        if (!this.read) {
            fromExcludedColumn();
        }
        return this.worksheetConfiguration.excludedRows();
    }

    @Override
    public Set<String> excludedColumns() {
        if (!this.read) {
            fromExcludedColumn();
        }
        return this.worksheetConfiguration.excludedColumns();
    }

    @Override
    public Set<Integer> metadataRows() {
        if (!this.read) {
            fromExcludedColumn();
        }
        return this.worksheetConfiguration.metadataRows();
    }

    @Override
    public Set<String> metadataColumns() {
        if (!this.read) {
            fromExcludedColumn();
        }
        return this.worksheetConfiguration.metadataColumns();
    }

    @Override
    public <T> T writtenTo(final Output<T> output) {
        if (!this.read) {
            fromExcludedColumn();
        }
        return this.worksheetConfiguration.writtenTo(output);
    }

    private void fromExcludedColumn() {
        final WorksheetNumberAndColumnName worksheetNumberAndColumnName =
            new WorksheetNumberAndColumnName(this.excludedColumnConfiguration);
        final String worksheetName;
        if (worksheetNumberAndColumnName.worksheetNumber().isEmpty()) {
            worksheetName = DEFAULT_NAME_PATTERN;
        } else {
            worksheetName = Pattern.quote(
                this.workbookFragments.localisedWorksheetNameFor(
                    Integer.parseUnsignedInt(worksheetNumberAndColumnName.worksheetNumber())
                )
            );
        }
        this.worksheetConfiguration = new WorksheetConfiguration.Default(
            worksheetName,
            Collections.emptySet(),
            Collections.singleton(worksheetNumberAndColumnName.columnName()),
            Collections.emptySet(),
            Collections.emptySet()
        );
        this.read = true;
    }

    static final class WorksheetNumberAndColumnName {
        private final String excludedColumnConfiguration;
        private String worksheetNumber;
        private String columnName;
        private boolean read;

        WorksheetNumberAndColumnName(final String excludedColumnConfiguration) {
            this.excludedColumnConfiguration = excludedColumnConfiguration;
        }

        String worksheetNumber() {
            if (!this.read) {
                read();
            }
            return this.worksheetNumber;
        }

        String columnName() {
            if (!this.read) {
                read();
            }
            return this.columnName;
        }

        void read() {
            final StringBuilder worksheetNumber = new StringBuilder();
            final StringBuilder columnName = new StringBuilder();
            final char[] chars = this.excludedColumnConfiguration.trim().toCharArray();
            for (final char ch : chars) {
                if (Character.isDigit(ch)) {
                    worksheetNumber.append(ch);
                } else {
                    columnName.append(ch);
                }
            }
            this.worksheetNumber = worksheetNumber.toString();
            this.columnName = columnName.toString();
            this.read = true;
        }
    }
}
