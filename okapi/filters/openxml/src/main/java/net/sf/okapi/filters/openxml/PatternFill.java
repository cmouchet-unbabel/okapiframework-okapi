/*
 * =============================================================================
 * Copyright (C) 2010-2021 by the Okapi Framework contributors
 * -----------------------------------------------------------------------------
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =============================================================================
 */
package net.sf.okapi.filters.openxml;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.util.LinkedList;

interface PatternFill {
    String NAME = "patternFill";
    ExcelColor backgroundColor();
    ExcelColor foregroundColor();
    void readWith(final XMLEventReader reader) throws XMLStreamException;
    Markup asMarkup();

    final class Empty implements PatternFill {
        @Override
        public ExcelColor backgroundColor() {
            return new ExcelColor.Empty();
        }

        @Override
        public ExcelColor foregroundColor() {
            return new ExcelColor.Empty();
        }

        @Override
        public void readWith(final XMLEventReader reader) throws XMLStreamException {
        }

        @Override
        public Markup asMarkup() {
            return new Markup.Empty();
        }
    }

    final class Default implements PatternFill {
        private final StartElement startElement;
        private ExcelColor backgroundColor;
        private ExcelColor foregroundColor;
        private EndElement endElement;

        Default(final StartElement startElement) {
            this.startElement = startElement;
        }

        @Override
        public ExcelColor backgroundColor() {
            if (null == this.backgroundColor) {
                this.backgroundColor = new ExcelColor.Empty();
            }
            return this.backgroundColor;
        }

        @Override
        public ExcelColor foregroundColor() {
            if (null == this.foregroundColor) {
                this.foregroundColor = new ExcelColor.Empty();
            }
            return this.foregroundColor;
        }

        @Override
        public void readWith(final XMLEventReader reader) throws XMLStreamException {
            while (reader.hasNext()) {
                final XMLEvent e = reader.nextEvent();
                if (e.isEndElement() && e.asEndElement().getName().equals(this.startElement.getName())) {
                    this.endElement = e.asEndElement();
                    break;
                }
                if (!e.isStartElement()) {
                    continue;
                }
                final StartElement se = e.asStartElement();
                if (ExcelColor.BACKGROUND.equals(se.getName().getLocalPart())) {
                    this.backgroundColor = new ExcelColor.Default(se);
                    this.backgroundColor.readWith(reader);
                    continue;
                }
                if (ExcelColor.FOREGROUND.equals(se.getName().getLocalPart())) {
                    this.foregroundColor = new ExcelColor.Default(se);
                    this.foregroundColor.readWith(reader);
                }
            }
        }

        @Override
        public Markup asMarkup() {
            if (null == this.endElement) {
                throw new IllegalStateException("The end element is not available");
            }
            final MarkupBuilder mb = new MarkupBuilder(new Markup.General(new LinkedList<>()));
            mb.add(this.startElement);
            mb.add(foregroundColor().asMarkup());
            mb.add(backgroundColor().asMarkup());
            mb.add(this.endElement);
            return mb.build();
        }
    }
}
