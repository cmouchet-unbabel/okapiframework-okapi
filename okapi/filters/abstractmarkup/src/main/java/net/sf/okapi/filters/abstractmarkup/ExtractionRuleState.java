/*===========================================================================
  Copyright (C) 2008-2009 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
===========================================================================*/

package net.sf.okapi.filters.abstractmarkup;

import net.sf.okapi.filters.abstractmarkup.config.TaggedFilterConfiguration.RULE_TYPE;

import java.util.EmptyStackException;
import java.util.ListIterator;
import java.util.Stack;

/**
 * Holds the current parser's rule state. State is maintained on separate
 * stacks for each type of {@link RULE_TYPE}
 *  
 * @author HargraveJE
 *
 */
public class ExtractionRuleState {

	/**
	 * This class carries the rule name, rule type and other information
	 */
	public static final class ExtractionRule {
		public String ruleName;
		public RULE_TYPE ruleType;
		public boolean ruleApplies;

		public ExtractionRule(String ruleName, RULE_TYPE ruleType, boolean rulesApplies) {
			this.ruleName = ruleName;
			this.ruleType = ruleType;
			this.ruleApplies = rulesApplies;
		}
	}
	
	// if no other rules are active, what do we do?
	private boolean excludeByDefault;

	// for rules without primary conditions
	private Stack<ExtractionRule> preserveWhiteSpaceRuleStack;
	private Stack<ExtractionRule> excludedIncludedRuleStack;
	private Stack<ExtractionRule> groupRuleStack;
	private Stack<ExtractionRule> textUnitRuleStack;
	private Stack<ExtractionRule> inlineRuleStack;
	private Stack<ExtractionRule> inlineExcludedIncludedRuleStack;

	/**
	 * 
	 */
	public ExtractionRuleState(boolean preserveWhitespace, boolean excludeByDefault) {
		reset(preserveWhitespace, excludeByDefault);
	}

	public void reset(boolean preserveWhitespace, boolean excludeByDefault) {
		this.excludeByDefault = excludeByDefault;
		preserveWhiteSpaceRuleStack = new Stack<>();
		// initial default whitespace rule
		pushPreserverWhitespaceRule(new ExtractionRule("", RULE_TYPE.PRESERVE_WHITESPACE, preserveWhitespace));
		excludedIncludedRuleStack = new Stack<>();
		groupRuleStack = new Stack<>();
		textUnitRuleStack = new Stack<>();
		inlineRuleStack = new Stack<>();
		inlineExcludedIncludedRuleStack = new Stack<>();
	}

	public boolean isExcludedState() {
		if (excludedIncludedRuleStack.isEmpty()) {
			return excludeByDefault;
		}

		ListIterator<ExtractionRule> ri = excludedIncludedRuleStack.listIterator(excludedIncludedRuleStack.size());
		while (ri.hasPrevious()) {
			ExtractionRule rt = ri.previous();
			if (rt.ruleApplies && rt.ruleType == RULE_TYPE.EXCLUDED_ELEMENT) {
				return true;
			}

			if (rt.ruleApplies && rt.ruleType == RULE_TYPE.INCLUDED_ELEMENT) {
				return false;
			}
		}
		
		return excludeByDefault;
	}

	public boolean isInlineExcludedState() {
		if (inlineExcludedIncludedRuleStack.isEmpty()) {
			return false;
		}

		ListIterator<ExtractionRule> ri = inlineExcludedIncludedRuleStack.listIterator(inlineExcludedIncludedRuleStack.size());
		while (ri.hasPrevious()) {
			ExtractionRule rt = ri.previous();
			if (rt.ruleApplies && rt.ruleType == RULE_TYPE.INLINE_EXCLUDED_ELEMENT) {
				return true;
			}

			if (rt.ruleApplies && rt.ruleType == RULE_TYPE.INLINE_INCLUDED_ELEMENT) {
				return false;
			}
		}

		return false;
	}
	
	public boolean isPreserveWhitespaceState() {
		if (preserveWhiteSpaceRuleStack.isEmpty()) {
			return false;
		}
		return preserveWhiteSpaceRuleStack.peek().ruleApplies;
	}

	public void pushPreserverWhitespaceRule(ExtractionRule rule) {
		preserveWhiteSpaceRuleStack.push(rule);
	}

	public void pushExcludedIncludedRule(ExtractionRule rule) {
		excludedIncludedRuleStack.push(rule);
	}

	public void pushGroupRule(ExtractionRule rule) {
		groupRuleStack.push(rule);
	}

	public void pushInlineRule(ExtractionRule rule) {
		inlineRuleStack.push(rule);
	}

	public void pushInlineExcludedIncludedRule(ExtractionRule rule) {
		inlineExcludedIncludedRuleStack.push(rule);
	}

	public void pushTextUnitRule(ExtractionRule rule) {
		textUnitRuleStack.push(rule);
	}

	public ExtractionRule popPreserverWhitespaceRule() {
		return preserveWhiteSpaceRuleStack.pop();
	}

	public ExtractionRule popExcludedIncludedRule() {
		return excludedIncludedRuleStack.pop();
	}

	public ExtractionRule popGroupRule() {
		return groupRuleStack.pop();
	}

	public ExtractionRule popTextUnitRule() {
		return textUnitRuleStack.pop();
	}
	
	public ExtractionRule popInlineRule() {
		return inlineRuleStack.pop();
	}

	public ExtractionRule popInlineExcludedIncludedRule() {
		return inlineExcludedIncludedRuleStack.pop();
	}

	public ExtractionRule peekPreserverWhitespaceRule() {
		try {
			return preserveWhiteSpaceRuleStack.peek();
		} catch (EmptyStackException e) {
			return null;
		}
	}

	public ExtractionRule peekExcludedIncludedRule() {
		try {
			return excludedIncludedRuleStack.peek();
		} catch (EmptyStackException e) {
			return null;
		}	}

	public ExtractionRule peekGroupRule() {
		try {
			return groupRuleStack.peek();
		} catch (EmptyStackException e) {
			return null;
		}
	}

	public ExtractionRule peekTextUnitRule() {
		try {
			return textUnitRuleStack.peek();
		} catch (EmptyStackException e) {
			return null;
		}
	}
	
	public ExtractionRule peekInlineRule() {
		try {
			return inlineRuleStack.peek();
		} catch (EmptyStackException e) {
			return null;
		}
	}

	public ExtractionRule peekExcludedIncludedInlineRule() {
		try {
			return inlineExcludedIncludedRuleStack.peek();
		} catch (EmptyStackException e) {
			return null;
		}
	}

	public void clearTextUnitRules() {
		textUnitRuleStack.clear();
	}
		
	public void clearInlineRules() {
		inlineRuleStack.clear();
	}
		
	public String getTextUnitElementName() {
		return peekTextUnitRule().ruleName == null ? "" : peekTextUnitRule().ruleName;
	}
}
